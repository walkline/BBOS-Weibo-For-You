package code;

import java.util.Hashtable;

import net.rim.device.api.util.Persistable;

public class AccessTokenSettings extends Hashtable implements Persistable
{
	private Hashtable _elements;
	
	public AccessTokenSettings() 
	{
		_elements=new Hashtable(4);
	}
	
	public void setToken(String accessToken) {_elements.put("access_token", accessToken);}
	public void setExpireIn(String expireIn) {_elements.put("expire_in", expireIn);}
	public void setUid(String uid) {_elements.put("uid", uid);}
	public void setExpiresTime(String expiresTime) {_elements.put("expires_time", expiresTime);}
	
	public String getToken() {return (String) _elements.get("access_token");}
	public String getExpireIn() {return (String) _elements.get("expire_in");}
	public String getUid() {return (String) _elements.get("uid");}
	public long getExpiresTime() {return Long.parseLong((String) _elements.get("expires_time"));}
}