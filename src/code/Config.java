package code;

import net.rim.device.api.ui.Color;
import net.rim.device.api.ui.Font;
import net.rim.device.api.ui.Ui;
import net.rim.device.api.ui.XYEdges;
import net.rim.device.api.ui.decor.Background;
import net.rim.device.api.ui.decor.BackgroundFactory;
import net.rim.device.api.ui.decor.Border;
import net.rim.device.api.ui.decor.BorderFactory;

public final class Config
{
	public static final String APP_TITLE="Walkline's Weibo";
	
	public static final int SCREEN_TYPE_FRIENDS=0;
	public static final int SCREEN_TYPE_FOLLOWERS=1;
	public static final int SCREEN_TYPE_STATUSES=2;
	public static final int SCREEN_TYPE_FAVORITES=3;
	
	public static final int IMAGE_TYPE_AVATAR=0;
	public static final int IMAGE_TYPE_STATUS=1;
	public static final int IMAGE_TYPE_RETWEET=2;
	
	public static final int STATUS_TYPE_FRIENDSTIMELINE=0;
	public static final int STATUS_TYPE_PUBLICTIMELINES=1;
	
	public static final int COMMENT_TYPE_MENTIONS=0;
	public static final int COMMENT_TYPE_TOME=1;
	public static final int COMMENT_TYPE_BYME=2;
	
	public static final Font FONT_SCREENNAME=Font.getDefault().derive(Font.PLAIN, Font.getDefault().getHeight(Ui.UNITS_pt)+1,Ui.UNITS_pt);
	public static final Font FONT_SOURCE=Font.getDefault().derive(Font.PLAIN, Font.getDefault().getHeight(Ui.UNITS_pt)-1,Ui.UNITS_pt);
	
	public static final Background bgColor_Gradient=BackgroundFactory.createLinearGradientBackground(Color.BLACK, Color.BLACK, Color.GRAY, Color.GRAY);
	public static final Border border_Transparent=BorderFactory.createRoundedBorder(new XYEdges(16,16,16,16), Color.BLACK, 200, Border.STYLE_FILLED);
	public static final Background bg_Transparent=BackgroundFactory.createSolidTransparentBackground(Color.BLACK, 200);
}