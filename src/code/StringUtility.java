package code;

import java.io.UnsupportedEncodingException;
import java.util.Enumeration;
import java.util.Hashtable;

import net.rim.device.api.ui.component.Dialog;

public class StringUtility
{
	public static String encodeUrl(Hashtable parameters)
	{
		if(parameters == null) {return "";}

		StringBuffer sb = new StringBuffer();
		boolean first = true;

		Enumeration keys=parameters.keys();
		while(keys.hasMoreElements())
		{
			if(first)
			{
				first = false;
			} else {
			    sb.append("&");
			}
			
			String _key=(String) keys.nextElement();
			String _value=(String) parameters.get(_key);
			
			if(_value!=null)
			{
			    try {
					sb.append(new String(_key.getBytes(), "UTF-8") + "=" + new String(_value.getBytes(), "UTF-8"));
				} catch (UnsupportedEncodingException e) {Dialog.alert(e.toString());}
			}
		}
		
		return sb.toString();
	}
	
	
	public static Hashtable decodeUrlCode(String s)
	{
		Hashtable params=new Hashtable();
		
		if(s!=null)
		{
			String arrayTemp[]=split(s, "?");
			String array[]=split(arrayTemp[1], "&");
			
			for(int i=0; i<array.length; i++)
			{
				String v[]=split(array[i], "=");
				try {
					params.put(new String(v[0].getBytes(), "UTF-8"), new String(v[1].getBytes(), "UTF-8"));
				} catch (UnsupportedEncodingException e) {Function.errorDialog(e.toString());}
			}
		}
		
		return params;
	}
	
	public static String[] split(String strString, String strDelimiter)
	{
		int iOccurrences = 0;
		int iIndexOfInnerString = 0;
		int iIndexOfDelimiter = 0;
		int iCounter = 0;

		if (strString == null) {throw new NullPointerException("Input string cannot be null.");}
		if (strDelimiter.length() <= 0 || strDelimiter == null) {throw new NullPointerException("Delimeter cannot be null or empty.");}

		if (strString.startsWith(strDelimiter)) {strString = strString.substring(strDelimiter.length());}
		if (!strString.endsWith(strDelimiter)) {strString += strDelimiter;}

		while((iIndexOfDelimiter= strString.indexOf(strDelimiter,iIndexOfInnerString))!=-1)
		{
			iOccurrences += 1;
			iIndexOfInnerString = iIndexOfDelimiter + strDelimiter.length();
		}

		String[] strArray = new String[iOccurrences];
		iIndexOfInnerString = 0;
		iIndexOfDelimiter = 0;

		while((iIndexOfDelimiter= strString.indexOf(strDelimiter,iIndexOfInnerString))!=-1)
		{
			strArray[iCounter] = strString.substring(iIndexOfInnerString, iIndexOfDelimiter);
			iIndexOfInnerString = iIndexOfDelimiter + strDelimiter.length();

			iCounter += 1;
		}
		
		return strArray;
	}
	
	public static String replace(String source, String pattern, String replacement)
	{	
		if (source == null) {return "";}

		StringBuffer sb = new StringBuffer();
		int idx = -1;
		int patIdx = 0;

		idx = source.indexOf(pattern, patIdx);
		while(idx!=-1)
		{
			sb.append(source.substring(patIdx, idx));
			sb.append(replacement);
			patIdx = idx + pattern.length();
			sb.append(source.substring(patIdx));

			idx = source.indexOf(pattern, patIdx);
		}
		//if (idx != -1)
		//{
		//	sb.append(source.substring(patIdx, idx));
		//	sb.append(replacement);
		//	patIdx = idx + pattern.length();
		//	sb.append(source.substring(patIdx));
		//}

        if (sb.length()==0)
        {
            return source;
        } else {
            return sb.toString();
        }
	}
	
	public static String replaceAll(String source, String pattern, String replacement)
	{    
	    if (source == null) {return "";}

	    StringBuffer sb = new StringBuffer();
	    int idx = 0;
	    String workingSource = source;
	    
	    while((idx=workingSource.indexOf(pattern, idx))!=-1)
	    {
	        sb.append(workingSource.substring(0, idx));
	        sb.append(replacement);
	        sb.append(workingSource.substring(idx + pattern.length()));
	        
	        workingSource = sb.toString();
	        sb.delete(0, sb.length());
	        idx += replacement.length();
	    }

	    return workingSource;
	}
	
	public static StringBuffer replace(String source, int startIndex, int endIndex, String replacement)
	{
		if(startIndex>endIndex) return new StringBuffer(source);
		
		StringBuffer sb=new StringBuffer();
		String tempBegin=source.substring(0, startIndex);
		String tempEnd=source.substring(endIndex);
		
		return sb.append(tempBegin).append(replacement).append(tempEnd);
	}
}