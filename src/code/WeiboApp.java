package code;

import java.io.IOException;

import net.rim.device.api.system.RadioException;
import net.rim.device.api.ui.UiApplication;
import screen.SplashScreen;

public class WeiboApp extends UiApplication
{
    public static void main(String[] args) throws IOException, RadioException
    {
        WeiboApp theApp = new WeiboApp();       
        theApp.enterEventDispatcher();
    }
    
    public WeiboApp() throws IOException, RadioException
    {        
    	new SplashScreen(UiApplication.getUiApplication());
    }    
}