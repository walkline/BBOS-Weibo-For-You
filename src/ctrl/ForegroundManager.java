package ctrl;

import net.rim.device.api.ui.Graphics;

public class ForegroundManager extends NegativeMarginVerticalFieldManager 
{
	int _backgroundColor=0xDDDDDD;
    public ForegroundManager(long style)
    {
        super(USE_ALL_HEIGHT | VERTICAL_SCROLL | VERTICAL_SCROLLBAR | style);
    }
    
    protected void paintBackground(Graphics g)
    {
        int oldColor = g.getColor();
        
        try {
            g.setColor(_backgroundColor);
            g.fillRect(0, getVerticalScroll(), getWidth(), getHeight());
        } finally {
            g.setColor(oldColor);
        }
    }
    
    public void setBackgroundColor(int color)
    {
    	_backgroundColor=color;
    	
    	invalidate();
    }
}