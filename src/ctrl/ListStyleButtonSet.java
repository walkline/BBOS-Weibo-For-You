package ctrl;

import net.rim.device.api.ui.*;
import net.rim.device.api.ui.container.*;

public class ListStyleButtonSet extends VerticalFieldManager 
{
	public ListStyleButtonSet()
    {
        super(NO_VERTICAL_SCROLL);
        setMargin(5, 5, 5, 5);
    }
    
    protected void sublayout( int maxWidth, int maxHeight )
    {
        super.sublayout( maxWidth, maxHeight );
        
        int numChildren = this.getFieldCount();
        if( numChildren > 0 )
        {
            if( numChildren == 1 )
            {
                Field child = getField( 0 );
                if(child instanceof UserInforListStyleButtonField)
                {
                    ((UserInforListStyleButtonField) child).setDrawPosition(UserInforListStyleButtonField.DRAWPOSITION_SINGLE);
                } else if(child instanceof StatusesListStyleButtonField)
                {
                	((StatusesListStyleButtonField) child).setDrawPosition(StatusesListStyleButtonField.DRAWPOSITION_SINGLE);
                }
            } else {
                int index = 0;
                Field child = getField( index );
                if(child instanceof UserInforListStyleButtonField)
                {
                    ( (UserInforListStyleButtonField) child).setDrawPosition( UserInforListStyleButtonField.DRAWPOSITION_TOP );
                } else if(child instanceof StatusesListStyleButtonField)
                {
                	((StatusesListStyleButtonField) child).setDrawPosition(StatusesListStyleButtonField.DRAWPOSITION_TOP);
                }

                for( index = 1; index < numChildren - 1 ; index++ )
                {
                    child = getField(index);
                    if(child instanceof UserInforListStyleButtonField)
                    {
                        ((UserInforListStyleButtonField) child).setDrawPosition(UserInforListStyleButtonField.DRAWPOSITION_MIDDLE);
                    } else if(child instanceof StatusesListStyleButtonField)
                    {
                    	((StatusesListStyleButtonField) child).setDrawPosition(StatusesListStyleButtonField.DRAWPOSITION_MIDDLE);
                    }
                }
                child = getField(index);
                if(child instanceof UserInforListStyleButtonField)
                {
                    ((UserInforListStyleButtonField) child).setDrawPosition(UserInforListStyleButtonField.DRAWPOSITION_BOTTOM);
                } else if(child instanceof StatusesListStyleButtonField)
                {
                	((StatusesListStyleButtonField) child).setDrawPosition(StatusesListStyleButtonField.DRAWPOSITION_BOTTOM);
                }
            }
        }
    }
}