package ctrl;

import net.rim.device.api.ui.Color;
import net.rim.device.api.ui.DrawTextParam;
import net.rim.device.api.ui.Graphics;
import net.rim.device.api.ui.component.ActiveRichTextField;

public class MyActiveRichTextField extends ActiveRichTextField
{
	int _forecolor;
	
	public MyActiveRichTextField(String text, int forecolor)
	{
		super(text, ActiveRichTextField.SCANFLAG_THREAD_ON_CREATE);
		_forecolor=forecolor;
	}
	
	public MyActiveRichTextField(String text, int forecolor, long style) {super(text, style); _forecolor=forecolor;}

	public void layout(int width, int height) {super.layout(width, height);}   
 
	public int drawText(Graphics g, int offset, int length, int x, int y, DrawTextParam drawTextParam)
	{
		if(drawTextParam.iUnderlineToBounds)
		{
			g.setColor(Color.GREEN);
		} else {
			g.setColor(_forecolor);
		}
		
		return super.drawText(g, offset, length, x, y, drawTextParam);
	}
}