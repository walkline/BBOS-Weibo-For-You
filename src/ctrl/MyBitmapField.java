package ctrl;

import net.rim.device.api.system.Bitmap;
import net.rim.device.api.system.Characters;
import net.rim.device.api.ui.Color;
import net.rim.device.api.ui.Graphics;
import net.rim.device.api.ui.TouchEvent;
import net.rim.device.api.ui.XYEdges;
import net.rim.device.api.ui.component.BitmapField;
import net.rim.device.api.ui.decor.Background;
import net.rim.device.api.ui.decor.BackgroundFactory;
import net.rim.device.api.ui.decor.Border;
import net.rim.device.api.ui.decor.BorderFactory;

public class MyBitmapField extends BitmapField
{
	public MyBitmapField(Bitmap bitmap)
	{
		this(bitmap, 0);
	}
	
	public MyBitmapField(Bitmap bitmap, long style)
	{
		super(bitmap, style);
		
		XYEdges edge=new XYEdges(5, 5, 5, 5);
		XYEdges colorNormal=new XYEdges(Color.DARKGRAY, Color.DARKGRAY, Color.DARKGRAY, Color.DARKGRAY);
		XYEdges colorFocus=new XYEdges(0x0A9000, 0x0A9000, 0x0A9000, 0x0A9000);
		Border borderNormal=BorderFactory.createSimpleBorder(edge, colorNormal, Border.STYLE_SOLID);
		Border borderFocus=BorderFactory.createSimpleBorder(edge, colorFocus, Border.STYLE_SOLID);
		Background background=BackgroundFactory.createSolidBackground(Color.WHITE);
		
		this.setBackground(background);
		this.setBorder(VISUAL_STATE_NORMAL, borderNormal);
		this.setBorder(VISUAL_STATE_FOCUS, borderFocus);
	}

	protected void paint(Graphics g)
	{
		super.paint(g);
	}
	
	protected void paintBackground(Graphics arg0) {}
	protected void drawFocus(Graphics graphics, boolean on) {}
	
	protected void layout(int width, int height)
	{
		super.layout(width, height);
		//super.layout(120, 60);
		//super.setExtent(120, 60);
	}
	
	protected boolean keyChar(char character, int status, int time) 
    {
        if(character == Characters.ENTER)
        {
            clickButton();
            return true;
        }
        
        return super.keyChar(character, status, time);
    }
    
    protected boolean navigationClick(int status, int time) 
    {
        clickButton(); 
        return true;    
    }
    
    protected boolean trackwheelClick(int status, int time)
    {        
        clickButton();    
        return true;
    }
    
    protected boolean invokeAction(int action) 
    {
        switch(action)
        {
            case ACTION_INVOKE:
                clickButton(); 
                return true;
        }
        
        return super.invokeAction(action);
    }
         
    public void clickButton() {fieldChangeNotify(0);}
       
    protected boolean touchEvent(TouchEvent message)
    {
        int x = message.getX(1);
        int y = message.getY(1);
        if(x < 0 || y < 0 || x > getExtent().width || y > getExtent().height) {return false;}
        
        switch(message.getEvent())
        {
        	case TouchEvent.CLICK:
                clickButton();
                return true;
        }
        
        return super.touchEvent(message);
    }

    public void setDirty(boolean dirty) {}
    public void setMuddy(boolean muddy) {}
}