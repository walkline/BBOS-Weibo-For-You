package ctrl;

import net.rim.device.api.io.transport.ConnectionFactory;
import net.rim.device.api.io.transport.TransportInfo;
import net.rim.device.api.io.transport.options.BisBOptions;
import net.rim.device.api.io.transport.options.TransportTypeOptions;

public class MyConnectionFactory extends ConnectionFactory
{
	public MyConnectionFactory()
	{
		setPreferredTransportTypes(new int[] {
										TransportInfo.TRANSPORT_TCP_WIFI,
										TransportInfo.TRANSPORT_BIS_B,
										//TransportInfo.TRANSPORT_MDS,
										TransportInfo.TRANSPORT_TCP_CELLULAR,
										TransportInfo.TRANSPORT_WAP2
										
		});
		
		setTransportTypeOptions(TransportInfo.TRANSPORT_BIS_B, new BisBOptions("nds-public"));
		setDisallowedTransportTypes(new int[] {TransportInfo.TRANSPORT_WAP});
		setAttemptsLimit(10);
		setConnectionTimeout(10000);
		setTimeLimit(10000);
	}
}