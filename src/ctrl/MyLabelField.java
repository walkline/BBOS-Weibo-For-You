package ctrl;

import net.rim.device.api.ui.Graphics;
import net.rim.device.api.ui.component.LabelField;

public class MyLabelField extends LabelField
{
	public MyLabelField(String text) {super(text);}

	public MyLabelField(String text, long style) {super(text, style);}

	public void layout(int width, int height) {super.layout(width, height);}   
        
	public void paint(Graphics g) {super.paint(g);}
}