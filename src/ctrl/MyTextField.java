package ctrl;

import net.rim.device.api.ui.Color;
import net.rim.device.api.ui.Font;
import net.rim.device.api.ui.Graphics;
import net.rim.device.api.ui.XYEdges;
import net.rim.device.api.ui.component.TextField;
import net.rim.device.api.ui.decor.Background;
import net.rim.device.api.ui.decor.BackgroundFactory;
import net.rim.device.api.ui.decor.Border;
import net.rim.device.api.ui.decor.BorderFactory;

public class MyTextField extends TextField
{
	public MyTextField()
	{
		super(TextField.USE_ALL_HEIGHT | TextField.USE_ALL_WIDTH);
		
		XYEdges edge=new XYEdges(4, 4, 4, 4);
		Border borderNormal=BorderFactory.createRoundedBorder(edge, 0xBBBBBB, Border.STYLE_SOLID);
		Border borderFocus=BorderFactory.createRoundedBorder(edge, 0x0A9000, Border.STYLE_SOLID);
		Background background=BackgroundFactory.createSolidBackground(Color.WHITE);
		
		this.setBackground(background);
		this.setBorder(VISUAL_STATE_NORMAL, borderNormal);
		this.setBorder(VISUAL_STATE_FOCUS, borderFocus);
	}

	protected void paint(Graphics g)
	{
		super.paint(g);
		
		String remainingWorld=Integer.toString(140-getTextLength())+"字";
		g.setColor(Color.GRAY);
		g.drawText(remainingWorld, getContentWidth()-Font.getDefault().getAdvance(remainingWorld), getContentHeight()+getPaddingBottom()-Font.getDefault().getHeight()-2);
	}
	
	protected void layout(int width, int height)
	{
		super.layout(width, height);
		super.setExtent(width, Font.getDefault().getHeight()*5);
	}
	
	protected void displayFieldFullMessage() {}
	
    public void setDirty(boolean dirty) {}
    public void setMuddy(boolean muddy) {}
}