package screen;

import net.rim.device.api.system.ApplicationDescriptor;
import net.rim.device.api.system.Characters;
import net.rim.device.api.ui.Font;
import net.rim.device.api.ui.Graphics;
import net.rim.device.api.ui.Ui;
import net.rim.device.api.ui.UiApplication;
import net.rim.device.api.ui.component.LabelField;
import net.rim.device.api.ui.component.SeparatorField;
import net.rim.device.api.ui.container.HorizontalFieldManager;
import net.rim.device.api.ui.container.PopupScreen;
import net.rim.device.api.ui.container.VerticalFieldManager;
import code.Config;

public class AboutScreen extends PopupScreen
{
	//private static ResourceBundle _resources = ResourceBundle.getBundle(BUNDLE_ID, BUNDLE_NAME);
	
	public AboutScreen()
	{
		super(new VerticalFieldManager(FOCUSABLE | NO_VERTICAL_SCROLL));
		
		//设置透明边框和背景
		setBorder(Config.border_Transparent);
		setBackground(Config.bg_Transparent);
		
		VerticalFieldManager vfm1=new VerticalFieldManager();
		VerticalFieldManager vfm2=new VerticalFieldManager(VERTICAL_SCROLL);
		HorizontalFieldManager horTitle1=new HorizontalFieldManager();
		//HorizontalFieldManager horTitle2=new HorizontalFieldManager();
		//VerticalButtonFieldSet vbf=new VerticalButtonFieldSet(USE_ALL_WIDTH);
		
    	LabelField about=new LabelField("帮助和关于", USE_ALL_WIDTH | LabelField.ELLIPSIS);
    	about.setFont(Font.getDefault().derive(Font.BOLD, Font.getDefault().getHeight(Ui.UNITS_pt)+2,Ui.UNITS_pt));
    	about.setPadding(0, 0, 1, 0);
    	
    	LabelField title=new LabelField(Config.APP_TITLE + " (OS6+)", USE_ALL_WIDTH | LabelField.ELLIPSIS);
    	title.setFont(Font.getDefault().derive(Font.PLAIN, Font.getDefault().getHeight(Ui.UNITS_pt)+1,Ui.UNITS_pt));
    	title.setPadding(3, 0, 1,0);

    	LabelField version=new LabelField("版本：" + ApplicationDescriptor.currentApplicationDescriptor().getVersion(), USE_ALL_WIDTH | LabelField.ELLIPSIS | LabelField.RIGHT);
    	version.setFont(Font.getDefault().derive(Font.PLAIN, Font.getDefault().getHeight(Ui.UNITS_pt)-1,Ui.UNITS_pt));

    	LabelField contact=new LabelField("联系方式：", USE_ALL_WIDTH | LabelField.ELLIPSIS);
    	contact.setFont(Font.getDefault().derive(Font.BOLD | Font.ITALIC, Font.getDefault().getHeight(Ui.UNITS_pt),Ui.UNITS_pt));

    	LabelField author=addLabel("\u2022 Walkline Wang");
    	LabelField email=addLabel("\u2022 walkline@gmail.com");
    	//HyperlinkButtonField email=new HyperlinkButtonField("walkline@gmail.com", 0x0000FF, 0xFFFFFF, 0x0000FF, 0, 0);
    	//LabelField shortcut=new LabelField(getResString(AS_SHORTCUT_KEYS), USE_ALL_WIDTH | LabelField.ELLIPSIS);
    	//shortcut.setFont(Font.getDefault().derive(Font.BOLD | Font.ITALIC, Font.getDefault().getHeight(Ui.UNITS_pt),Ui.UNITS_pt));
    	
    	//LabelField shortS=addLabel(getResString(AS_TIP_OF_KEY_S));
    	//LabelField shortF=addLabel(getResString(AS_TIP_OF_KEY_F));
    	//LabelField shortV=addLabel(getResString(AS_TIP_OF_KEY_V));
    	//LabelField shortE=addLabel(getResString(AS_TIP_OF_KEY_E));
    	//LabelField shortA=addLabel(getResString(AS_TIP_OF_KEY_A));
    	
    	//ButtonField btnWriteAReview=new ButtonField(getResString(MENU_BBWORLD_WRITE_A_REVIEW), ButtonField.NEVER_DIRTY | ButtonField.CONSUME_CLICK);
    	//btnWriteAReview.setChangeListener(new FieldChangeListener() {
		//	public void fieldChanged(Field field, int context)
		//	{
		//		try
	    //       {
	    //          openAppWorld("25682903");
	    //        } catch(final Exception e)
	    //        {
	    //            UiApplication.getUiApplication().invokeLater(new Runnable()
	    //            {
	    //                public void run()
	    //                {
	    //                	if(e instanceof ContentHandlerException)
	    //                	{
	    //                		Dialog.alert("BlackBerry World is not installed!");
	    //                	} else {
	    //                		Dialog.alert( "Problems opening App World: " + e.getMessage() );
	    //                	}
	    //                }
	    //            });
	    //        }
		//	}
		//});
    	
    	//ButtonField btnBrowseOtherApps=new ButtonField(getResString(MENU_BBWORLD_BROWSE_OTHER_APPS), ButtonField.NEVER_DIRTY | ButtonField.CONSUME_CLICK);
    	//btnBrowseOtherApps.setChangeListener(new FieldChangeListener() {
		//	public void fieldChanged(Field field, int context)
		//	{
		//		BrowserSession browser=Browser.getDefaultSession();
	    //		browser.displayPage("http://appworld.blackberry.com/webstore/vendor/69061");
		//	}
		//});
    	
    	vfm1.add(about);
    	vfm1.add(new SeparatorField());
    	vfm1.add(title);
    	vfm1.add(version);
    	//vfm1.add(new LabelField("", LabelField.FOCUSABLE));

    	horTitle1.add(contact);
    	//horTitle1.add(new LabelField("", LabelField.FOCUSABLE));
    	vfm2.add(horTitle1);
    	vfm2.add(author);
    	vfm2.add(email);
    	//vfm2.add(new LabelField());
    	
    	//horTitle2.add(shortcut);
    	//horTitle2.add(new LabelField("", LabelField.FOCUSABLE));
    	//vfm2.add(horTitle2);
    	//vfm2.add(shortS);
    	//vfm2.add(shortF);
    	//vfm2.add(shortV);
    	//vfm2.add(shortE);
    	//vfm2.add(shortA);
    	//vfm2.add(new LabelField());
    	//vbf.add(btnWriteAReview);
    	//vbf.add(btnBrowseOtherApps);
    	//vfm2.add(vbf);
    	
    	add(vfm1);
    	add(vfm2);
	}
	
	//private String getResString(int key) {return _resources.getString(key);}
	
	private LabelField addLabel(String label)
	{
		return new LabelField(label, USE_ALL_WIDTH | LabelField.ELLIPSIS);
	}

	protected void paintBackground(Graphics g) {}

	protected boolean keyChar(char key, int status, int time)
	{
		if(key==Characters.ESCAPE)
		{
			UiApplication.getUiApplication().popScreen(this);
			return true;
		}
		
		return super.keyChar(key, status, time);
	}
/*	
	protected boolean keyChar(char key, int status, int time)
	{
		UiApplication.getUiApplication().popScreen(this);
		return true;
	}
	
	protected boolean navigationClick( int status, int time ) 
    {
		UiApplication.getUiApplication().popScreen(this);
        return true;    
    }
	
	protected boolean trackwheelClick( int status, int time )
    {        
		UiApplication.getUiApplication().popScreen(this);    
        return true;
    }
	
	protected boolean touchEvent(TouchEvent message)
	{
		if(message.getEvent()==TouchEvent.CLICK)
		{
			UiApplication.getUiApplication().popScreen(this);
			return true;	
		}
		
		return super.touchEvent(message);
	}
*/	
}