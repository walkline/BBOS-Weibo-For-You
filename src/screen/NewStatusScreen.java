package screen;

import net.rim.device.api.system.DeviceInfo;
import net.rim.device.api.system.EncodedImage;
import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.FieldChangeListener;
import net.rim.device.api.ui.Graphics;
import net.rim.device.api.ui.component.BitmapField;
import net.rim.device.api.ui.component.ButtonField;
import net.rim.device.api.ui.component.LabelField;
import net.rim.device.api.ui.container.HorizontalFieldManager;
import net.rim.device.api.ui.container.MainScreen;
import net.rim.device.api.ui.picker.FilePicker;
import weibo.Timeline;
import weibo.http.AccessToken;
import weibo.http.ImageItem;
import weibo.model.Status;
import weibo.model.WeiboException;
import code.Function;
import ctrl.ForegroundManager;
import ctrl.MyTextField;

public class NewStatusScreen extends MainScreen implements FilePicker.Listener
{
	AccessToken _accessToken;
	MyTextField statusField;
	ForegroundManager _foreground=new ForegroundManager(0);
	BitmapField _bitmapField=new BitmapField();
	
	FilePicker _filePicker;
	ImageItem _imageItem;
	
	public NewStatusScreen(AccessToken accessToken)
	{
		super(NO_VERTICAL_SCROLL | USE_ALL_HEIGHT);

		_accessToken=accessToken;
		
        _filePicker = FilePicker.getInstance();
        _filePicker.setTitle("Choose file");
        if(!DeviceInfo.isSimulator()) {_filePicker.setView(FilePicker.VIEW_PICTURES);}
        _filePicker.setFilter(".png:.gif:.jpg");

		HorizontalFieldManager hfm=new HorizontalFieldManager(FIELD_HCENTER);
		hfm.add(new LabelField("发表微博"));
		setTitle(hfm);
		
		statusField=new MyTextField();
		statusField.setMaxSize(200);
		statusField.setPadding(3, 3, 3, 3);
		statusField.setMargin(10, 3, 3, 3);
		
		ButtonField updateButton=new ButtonField("发送文字", ButtonField.CONSUME_CLICK | ButtonField.NEVER_DIRTY);
		updateButton.setChangeListener(new FieldChangeListener()
		{
			public void fieldChanged(Field field, int context)
			{
				if(updateStatus(statusField.getText()))
				{
					Function.errorDialog("发表成功！");
					onClose();
				}
			}
		});
		updateButton.setMargin(5, 5, 5, 10);

		HorizontalFieldManager hfmToolbar=new HorizontalFieldManager();
		ButtonField uploadButton=new ButtonField("发送图片", ButtonField.CONSUME_CLICK | ButtonField.NEVER_DIRTY);
		uploadButton.setChangeListener(new FieldChangeListener()
		{
			public void fieldChanged(Field field, int context)
			{
				if(uploadStatus(statusField.getText()))
				{
					Function.errorDialog("发表成功！");
					onClose();
				}
			}
		});
		uploadButton.setMargin(5, 5, 5, 10);

		ButtonField addImage=new ButtonField("添加图片", ButtonField.CONSUME_CLICK | ButtonField.NEVER_DIRTY);
		addImage.setChangeListener(new FieldChangeListener()
		{
			public void fieldChanged(Field field, int context)
			{
				_filePicker.show();
			}
		});
		addImage.setMargin(5, 5, 5, 0);
		
		hfmToolbar.add(uploadButton);
		hfmToolbar.add(addImage);
		
		HorizontalFieldManager hfmImage=new HorizontalFieldManager(FIELD_HCENTER);
		hfmImage.add(_bitmapField);
		
		_foreground.add(statusField);
		_foreground.add(updateButton);
		_foreground.add(hfmToolbar);
		_foreground.add(hfmImage);
		
		add(_foreground);
		
		_filePicker.setListener(this);
	}
	
	private boolean updateStatus(String text)
	{
		boolean returnValue=false;
		
		Timeline tm = new Timeline(_accessToken.getToken());

		try {
			Status status = tm.UpdateStatus(text);
			
			if(status!=null) {returnValue=true;}
		} catch (WeiboException e) {}	
	
		return returnValue;
	}

	private boolean uploadStatus(String text)
	{
		boolean returnValue=false;
		
		try {
			Timeline tm = new Timeline(_accessToken.getToken());
			
			if(_imageItem!=null)
			{
				Function.errorDialog("Image file information\n\nFilename: " + _imageItem.getFilename() + "\nFile type: " + _imageItem.getContentType() + "\nWidth: " + _imageItem.getImageWidth() + "\nHeight: " + _imageItem.getImageHeight() + "\nData size: " + _imageItem.getContent().length + " Bytes");
				Status status = tm.UploadStatus(statusField.getText(), _imageItem);
				if(status!=null) {returnValue=true;}
			} else {
				Function.errorDialog("请选择一张图片，然后再试！");
			}
		} catch (Exception e1) {Function.errorDialog(e1.toString());}
		
		return returnValue;
	}
	
	protected boolean onSavePrompt() {return true;}

	public void selectionDone(String selected)
	{
		if(selected!=null && selected.length()>0)
		{
			try {
				_imageItem = new ImageItem(selected);
				
				if(_imageItem!=null)
				{
					EncodedImage image=EncodedImage.createEncodedImage(_imageItem.getContent(), 0, _imageItem.getContent().length);

					_bitmapField.setBitmap(Function.bestFit(image, 200, 200));
				}
			} catch (WeiboException e) {Function.errorDialog(e.toString());}	
		} else {
			_bitmapField.setBitmap(null); 
		}
		//Function.errorDialog(hfmImage.getFieldCount()+"");
		//if(hfmImage.getFieldCount()<=0) {hfmImage.add(_bitmapField);}
	}
}