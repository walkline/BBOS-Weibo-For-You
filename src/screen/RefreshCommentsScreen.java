package screen;

import net.rim.device.api.system.Application;
import net.rim.device.api.system.Characters;
import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.UiApplication;
import net.rim.device.api.ui.component.LabelField;
import net.rim.device.api.ui.container.PopupScreen;
import net.rim.device.api.ui.container.VerticalFieldManager;
import weibo.Comments;
import weibo.http.AccessToken;
import weibo.model.CommentWapper;
import weibo.model.WeiboException;
import code.Config;
import code.Function;

/**
 * 刷新微博消息
 * @author Administrator
 */
public class RefreshCommentsScreen extends PopupScreen
{
	AccessToken _accessToken;
	String _uid;
	CommentWapper _comments=null;

	Thread thread=null;

	/**
	 * 刷新显示指定的用户消息
	 * @param page 指定显示的页码
	 * @param status_type 要显示的微博类型
	 */
	public RefreshCommentsScreen(AccessToken accessToken, final int page, final int comment_type)
	{
		super(new VerticalFieldManager());
		
		add(new LabelField("Please wait....", Field.FIELD_HCENTER));
		
		_accessToken=accessToken;

		thread=new Thread() 
		{
			public void run()
			{
				try {
					Comments cm = new Comments(_accessToken.getToken());

                	switch (comment_type) 
                	{
						case Config.COMMENT_TYPE_MENTIONS: _comments=cm.getCommentMentions(page, 30); break;
						case Config.COMMENT_TYPE_TOME: _comments=cm.getCommentToMe(page, 30); break;
						case Config.COMMENT_TYPE_BYME: _comments=cm.getCommentByMe(page, 30); break;
					}
				} catch (WeiboException e) {Function.errorDialog(e.toString());}
				
				Application.getApplication().invokeLater(new Runnable()
				{
					public void run() {if(thread!=null) {onClose();}}
				});
			}
		};
		thread.start();
	}
	
	public CommentWapper getComments() {return _comments;}
	
	public boolean onClose()
	{
		if(thread!=null) {try {thread=null;} catch (Exception e) {}}

		try {
			UiApplication.getUiApplication().popScreen(this);					
		} catch (Exception e) {Function.errorDialog(e.toString());}
		
		return true;
	}
	
	protected boolean keyChar(char key, int status, int time)
	{
		if(key==Characters.ESCAPE)
		{
			onClose();
			
			return true;
		}
		
		return super.keyChar(key, status, time);
	}
}