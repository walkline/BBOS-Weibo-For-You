package screen;

import net.rim.device.api.system.Application;
import net.rim.device.api.system.Characters;
import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.UiApplication;
import net.rim.device.api.ui.component.LabelField;
import net.rim.device.api.ui.container.PopupScreen;
import net.rim.device.api.ui.container.VerticalFieldManager;
import weibo.Timeline;
import weibo.http.AccessToken;
import weibo.model.StatusWapper;
import weibo.model.WeiboException;
import code.Config;
import code.Function;

/**
 * 刷新微博内容
 * @author Administrator
 *
 */
public class RefreshStatusesScreen extends PopupScreen
{
	AccessToken _accessToken;
	String _uid;
	StatusWapper _statuses=null;

	Thread thread=null;

	/**
	 * 刷新显示指定用户微博内容
	 * @param page 指定显示的页码
	 * @param status_type 要显示的微博类型
	 */
	public RefreshStatusesScreen(AccessToken accessToken, final int page, final int status_type)
	{
		super(new VerticalFieldManager());
		
		add(new LabelField("Please wait....", Field.FIELD_HCENTER));
		
		_accessToken=accessToken;

		thread=new Thread() 
		{
			public void run()
			{
				try {
					Timeline tm=new Timeline(_accessToken.getToken());
                    
                    switch (status_type)
                    {
						case Config.STATUS_TYPE_FRIENDSTIMELINE: _statuses=tm.getFriendsTimeline(page, 20); break;
						case Config.STATUS_TYPE_PUBLICTIMELINES: _statuses=tm.getPublicTimeline(50); break;
					}
				} catch (WeiboException e) {Function.errorDialog(e.toString());}
				
				Application.getApplication().invokeLater(new Runnable()
				{
					public void run() {if(thread!=null) {onClose();}}
				});
			}
		};
		thread.start();
	}
	
	public StatusWapper getStatuses() {return _statuses;}
	
	public boolean onClose()
	{
		if(thread!=null) {try {thread=null;} catch (Exception e) {}}

		try {
			UiApplication.getUiApplication().popScreen(this);					
		} catch (Exception e) {Function.errorDialog(e.toString());}
		
		return true;
	}
	
	protected boolean keyChar(char key, int status, int time)
	{
		if(key==Characters.ESCAPE)
		{
			onClose();
			
			return true;
		}
		
		return super.keyChar(key, status, time);
	}
}