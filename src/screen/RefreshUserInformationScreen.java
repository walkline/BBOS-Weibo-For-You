package screen;

import java.io.IOException;
import java.io.InputStream;
import java.util.Vector;

import javax.microedition.io.Connector;
import javax.microedition.io.HttpConnection;

import net.rim.device.api.io.IOCancelledException;
import net.rim.device.api.io.IOUtilities;
import net.rim.device.api.io.transport.ConnectionDescriptor;
import net.rim.device.api.system.Application;
import net.rim.device.api.system.Characters;
import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.UiApplication;
import net.rim.device.api.ui.component.LabelField;
import net.rim.device.api.ui.container.PopupScreen;
import net.rim.device.api.ui.container.VerticalFieldManager;
import weibo.Trend;
import weibo.Users;
import weibo.http.AccessToken;
import weibo.model.User;
import weibo.model.WeiboException;
import code.Function;
import ctrl.MyConnectionFactory;

/**
 * 刷新显示用户信息
 * @author Administrator
 *
 */
public class RefreshUserInformationScreen extends PopupScreen
{
	AccessToken _accessToken;
	String _uid;
	User userInformaion=null;
	byte[] avatarData=null;
	Thread thread=null;
	Users user;

	/**
	 * 刷新显示已登录用户信息
	 * @param accessToken 令牌类
	 */
	public RefreshUserInformationScreen(AccessToken accessToken)
	{
		this(accessToken, accessToken.getUID());
	}
	
	/**
	 * 刷新显示指定id用户信息
	 * @param accessToken 令牌类
	 * @param uid 用户id
	 */
	public RefreshUserInformationScreen(AccessToken accessToken, String uid)
	{
		super(new VerticalFieldManager());
		
		add(new LabelField("Please wait....", Field.FIELD_HCENTER));
		
		_accessToken=accessToken;
		_uid=uid;
		user=new Users(_accessToken.getToken());
		thread=new Thread() 
		{
			public void run()
			{
				try {
					userInformaion = user.showUserById(_uid);
				} catch (WeiboException e) {Function.errorDialog(e.toString());}
				
				if(userInformaion!=null)
				{
	                MyConnectionFactory factory=new MyConnectionFactory();
	                ConnectionDescriptor descriptor=factory.getConnection(userInformaion.getProfileImageUrl());
	                if(descriptor==null) {return;}
	                
	                HttpConnection httpConnection=(HttpConnection) descriptor.getConnection();
	                InputStream inputStream=null;
	                
					try {
						inputStream=httpConnection.openInputStream();

						if(inputStream.available()>0)
						{
							avatarData=IOUtilities.streamToBytes(inputStream);
							
							if(avatarData.length==httpConnection.getLength())
							{
								userInformaion.setAvatarData(avatarData);	
							}
						}
						
						Trend tm = new Trend(_accessToken.getToken());
						Vector trends = null;
						try
						{
							trends = tm.getTrends(userInformaion.getId());
							
							userInformaion.setTrends(trends);
						} catch (WeiboException e) {}
					} catch (IOException e) {}
				}
				
				Application.getApplication().invokeLater(new Runnable()
				{
					public void run() {if(thread!=null) {onClose();}}
				});
			}
		};
		thread.start();
	}
	
	public User getUser() {return userInformaion;}
	
	public boolean onClose()
	{
		if(user!=null) {try {user=null;} catch (Exception e) {}}
		
		if(thread!=null) {try {thread=null;} catch (Exception e) {}}

		try {
			UiApplication.getUiApplication().popScreen(this);					
		} catch (Exception e) {Function.errorDialog(e.toString());}
		
		return true;
	}
	
	protected boolean keyChar(char key, int status, int time)
	{
		if(key==Characters.ESCAPE)
		{
			onClose();
			
			return true;
		}
		
		return super.keyChar(key, status, time);
	}
}