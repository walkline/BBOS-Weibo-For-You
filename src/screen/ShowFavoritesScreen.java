package screen;

import java.util.Vector;

import net.rim.device.api.ui.UiApplication;
import net.rim.device.api.ui.component.LabelField;
import net.rim.device.api.ui.container.HorizontalFieldManager;
import net.rim.device.api.ui.container.MainScreen;
import weibo.Favorite;
import weibo.http.AccessToken;
import weibo.model.Favorites;
//import weibo.model.Oauth2AccessToken;
import weibo.model.Status;
import weibo.model.User;
import weibo.model.WeiboException;
import code.Function;
import ctrl.ForegroundManager;
import ctrl.ListStyleButtonSet;
import ctrl.StatusesListStyleButtonField;

public class ShowFavoritesScreen extends MainScreen
{
	AccessToken _accessToken;
	User _user;
	String _uid;
	Vector favors;
	
	ForegroundManager _foreground=new ForegroundManager(0);
	ListStyleButtonSet _buttonSet=new ListStyleButtonSet();
	StatusesListStyleButtonField _link;
	
    ConnectionThread _connectionThread = new ConnectionThread();
	
	public ShowFavoritesScreen(AccessToken accessToken, User user)
	{
		super(NO_VERTICAL_SCROLL | USE_ALL_HEIGHT);
		
        if(!_connectionThread.isStarted()) {_connectionThread.start();}
        
		_accessToken=accessToken;
		_user=user;
		_uid=_user.getId();
		
		HorizontalFieldManager hfm=new HorizontalFieldManager(FIELD_HCENTER);
		hfm.add(new LabelField(_user.getScreenName()+"的收藏列表"));
		setTitle(hfm);
		
		_buttonSet.add(new LabelField("Please wait....", USE_ALL_WIDTH));
		_foreground.add(_buttonSet);
		
		add(_foreground);
		
		_connectionThread.fetch();	
	}
	
	public boolean onClose()
	{
		_connectionThread.stop();
		
		return super.onClose();
	}
	
	class ConnectionThread extends Thread
    {
        private static final int TIMEOUT = 500;
        
        private volatile boolean _fetchStarted = false;
        private volatile boolean _stop = false;

        private String getAccessToken() {return _accessToken.getToken();}
        
        private boolean isStarted() {return _fetchStarted;}            
     
        private void fetch() {_fetchStarted = true;}

        private void stop() {_stop = true;}

        public void run()
        {
            for(;;)
            {
                while( !_fetchStarted && !_stop)  
                {
                    try 
                    {
                        sleep(TIMEOUT);
                    } 
                    catch (InterruptedException e) {Function.errorDialog("Thread#sleep(long) threw " + e.toString());}
                }
                
                if(_stop) {return;}
                
        		Favorite fm = new Favorite(getAccessToken());

        		try {
        			favors = fm.getFavorites();

					synchronized(UiApplication.getEventLock())
					{
						UiApplication.getUiApplication().invokeLater(new Runnable()
						{
							public void run()
							{
			        			Status status;
			        			
			        			for(int i=0; i<favors.size(); i++)
			        			{
									if (_stop) {return;}
									
									status=((Favorites) favors.elementAt(i)).getStatus();
									_link=new StatusesListStyleButtonField(status);
									_buttonSet.add(_link);
			        			}
							};
						});
					}
				} catch (WeiboException e) {Function.errorDialog(e.toString());}
        		
				UiApplication.getUiApplication().invokeLater(new Runnable()
				{
					public void run()
					{
						if(_buttonSet.getField(0) instanceof LabelField) {_buttonSet.delete(_buttonSet.getField(0));}							
					}
				});

	            _fetchStarted = false;     
			}
        }
    }
}