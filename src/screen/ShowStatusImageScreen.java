package screen;

import java.io.IOException;
import java.io.InputStream;

import javax.microedition.io.HttpConnection;

import net.rim.device.api.io.IOUtilities;
import net.rim.device.api.io.transport.ConnectionDescriptor;
import net.rim.device.api.system.Application;
import net.rim.device.api.system.Bitmap;
import net.rim.device.api.system.Characters;
import net.rim.device.api.system.Display;
import net.rim.device.api.system.EncodedImage;
import net.rim.device.api.system.GIFEncodedImage;
import net.rim.device.api.ui.Color;
import net.rim.device.api.ui.UiApplication;
import net.rim.device.api.ui.component.BitmapField;
import net.rim.device.api.ui.component.LabelField;
import net.rim.device.api.ui.container.FullScreen;
import net.rim.device.api.ui.container.VerticalFieldManager;
import net.rim.device.api.ui.decor.BackgroundFactory;
import code.Function;
import ctrl.MyConnectionFactory;
import ctrl.ScrollableImageField;

/**
 * 显示微博图片
 * @author Walkline
 */
public class ShowStatusImageScreen extends FullScreen
{
	Thread thread=null;
	ScrollableImageField _bitmapField;//=new ScrollableImageField();
	BitmapField _bitmapFieldSmall;
	VerticalFieldManager _foreground=new VerticalFieldManager();
	
	String _url;
	private byte[] data;
	boolean isGif;
	
	/**
	 * 弹出窗口显示用户大头像图片
	 * @param avatarUrl 大头像图片URL，取自用户信息
	 */
	public ShowStatusImageScreen(String url)
	{
		super(NO_VERTICAL_SCROLL | USE_ALL_HEIGHT);
		setBackground(BackgroundFactory.createSolidBackground(Color.BLACK));
		
		_url=url;
		
		_bitmapFieldSmall=new BitmapField(Bitmap.getBitmapResource("avatar_large.png"), LabelField.VCENTER | LabelField.HCENTER);
		_foreground.add(_bitmapFieldSmall);

		add(_foreground);

		Thread thread=new Thread() 
		{
			public void run()
			{
                MyConnectionFactory factory=new MyConnectionFactory();
                ConnectionDescriptor descriptor=factory.getConnection(_url);
                if(descriptor==null) {return;}
                
                HttpConnection httpConnection=(HttpConnection) descriptor.getConnection();
                InputStream inputStream=null;
                
				try {
					isGif=httpConnection.getType().equals("image/gif");
					
					inputStream=httpConnection.openInputStream();

					if(inputStream.available()>0)
					{
						data=IOUtilities.streamToBytes(inputStream);

						if(data.length==httpConnection.getLength())
						{
							synchronized(Application.getEventLock())
							{
								Application.getApplication().invokeLater(new Runnable()
								{
									public void run()
									{
										if(isGif)
										{
											GIFEncodedImage image=(GIFEncodedImage) GIFEncodedImage.createEncodedImage(data, 0, -1);
											_bitmapFieldSmall.setImage(image);
										} else {
											EncodedImage image=EncodedImage.createEncodedImage(data, 0, -1);
										
											if(image.getWidth()<=Display.getWidth() && image.getHeight()<=Display.getHeight())
											{
												_bitmapFieldSmall.setImage(image);
											} else {
												_foreground.deleteAll();
												_bitmapField=new ScrollableImageField(image);
												_foreground.add(_bitmapField);
											}
										}
									}
								});		
							}		
						}
					}
				} catch (IOException e) {Function.errorDialog(e.toString());}
				  finally {
					  try {if(inputStream!=null) {inputStream.close();}} catch (Exception e2) {}
					  try {if(httpConnection!=null) {httpConnection.close();}} catch (Exception e2) {}}
				}
		};
		thread.start();
	}
	
	public boolean onClose()
	{
		if(thread!=null) {try {thread=null;} catch (Exception e) {}}
		
		try {UiApplication.getUiApplication().popScreen(this);} catch (Exception e) {}
		
		return true;
	}
	
	protected boolean keyChar(char key, int status, int time)
	{
		if(key==Characters.ESCAPE)
		{
			onClose();
			
			return true;
		}
		
		return super.keyChar(key, status, time);
	}
}