package screen;

import java.util.Vector;

import net.rim.device.api.system.Bitmap;
import net.rim.device.api.system.Characters;
import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.FieldChangeListener;
import net.rim.device.api.ui.MenuItem;
import net.rim.device.api.ui.UiApplication;
import net.rim.device.api.ui.component.LabelField;
import net.rim.device.api.ui.component.Menu;
import net.rim.device.api.ui.container.HorizontalFieldManager;
import net.rim.device.api.ui.container.MainScreen;
import net.rim.device.api.util.StringProvider;
import weibo.http.AccessToken;
import weibo.model.User;
import weibo.model.UserTrend;
import code.Config;
import code.Function;
import ctrl.ForegroundManager;
import ctrl.ListStyleButtonSet;
import ctrl.UserInforListStyleButtonField;

public class UserInformationScreen extends MainScreen
{
	AccessToken _accessToken;
	String _uid;
	User _user;
	//User userInformaion;
	ForegroundManager _foreground=new ForegroundManager(0);
	ListStyleButtonSet _buttonSet1=new ListStyleButtonSet();
	ListStyleButtonSet _buttonSet2=new ListStyleButtonSet();
	
	public UserInformationScreen(AccessToken accessToken)
	{
		this(accessToken, accessToken.getUID());
	}
	
	public UserInformationScreen(AccessToken accessToken, String uid)
	{
		super(NO_VERTICAL_SCROLL | USE_ALL_HEIGHT);

		_accessToken=accessToken;
		_uid=uid;

		HorizontalFieldManager hfm=new HorizontalFieldManager(FIELD_HCENTER);
		hfm.add(new LabelField("个人信息"));
		setTitle(hfm);

		createUI();
		
		add(_foreground);

		UiApplication.getUiApplication().invokeLater(new Runnable()
		{
			public void run() {menuRefreshInformation.run();}
		});
	}
	
	public UserInformationScreen(AccessToken accessToken, User user)
	{
		super(NO_VERTICAL_SCROLL | USE_ALL_HEIGHT);

		_accessToken=accessToken;
		_uid=user.getId();
		_user=user;
		
		HorizontalFieldManager hfm=new HorizontalFieldManager(FIELD_HCENTER);
		hfm.add(new LabelField(_user.getScreenName() + "的主页"));
		setTitle(hfm);

		createUI();
		
		add(_foreground);

		UiApplication.getUiApplication().invokeLater(new Runnable()
		{
			public void run() {refreshUI(_user);}//{menuRefreshInformation.run();}
		});
	}
	
	private void createUI()
	{
		UserInforListStyleButtonField link;
		
		link=new UserInforListStyleButtonField(Bitmap.getBitmapResource("avatar.png"), "", "", "", null);
		_buttonSet1.add(link);
		
		link=new UserInforListStyleButtonField("微号：", "无", null);
		link.setEnabled(false);
		_buttonSet1.add(link);

		link=new UserInforListStyleButtonField("注册时间：", "无", null);
		link.setEnabled(false);
		_buttonSet1.add(link);
		
		link=new UserInforListStyleButtonField("城市：", "无", null);
		link.setEnabled(false);
		_buttonSet1.add(link);

		link=new UserInforListStyleButtonField("个人主页：", null, "无");
		link.setEnabled(false);
		_buttonSet1.add(link);

		link=new UserInforListStyleButtonField("个人简介：", null, "无");
		link.setEnabled(false);
		_buttonSet1.add(link);
		
		link=new UserInforListStyleButtonField("微博：", "0", 0);
		_buttonSet2.add(link);
		
		link=new UserInforListStyleButtonField("关注：", "0", 0);
		_buttonSet2.add(link);
		
		link=new UserInforListStyleButtonField("粉丝(互粉)：", "0", 0);
		_buttonSet2.add(link);
		
		link=new UserInforListStyleButtonField("收藏：", "0", 0);
		_buttonSet2.add(link);
		
		link=new UserInforListStyleButtonField("话题：", "0", 0);
		_buttonSet2.add(link);
		
		_foreground.add(_buttonSet1);
		_foreground.add(_buttonSet2);
	}
	
	private void refreshUI(final User user)
	{
		HorizontalFieldManager hfm=new HorizontalFieldManager(FIELD_HCENTER);
		hfm.add(new LabelField(user.getScreenName()+"的主页"));
		setTitle(hfm);
		
		try {
			UserInforListStyleButtonField link=(UserInforListStyleButtonField) _buttonSet1.getField(0);
			link.setAvatarIcon(user.getAvatarData()); //小头像
			link.setTitleText(user.getScreenName()); //姓名
			link.setValueText(user.getUserOnlineStatus()); //在线状态
			link.setDescription(user.getUserGender()); //性别
			link.setChangeListener(null);
			link.setChangeListener(new FieldChangeListener()
			{
				public void fieldChanged(Field field, int context) 
				{
					UiApplication.getUiApplication().pushModalScreen(new ShowUserAvatarImageScreen(user.getAvatarLarge()));
				}
			});
			
			link=(UserInforListStyleButtonField) _buttonSet1.getField(1);
			link.setValueText(user.getWeihao().equals("") ? "无" : user.getWeihao()); //微号

			link=(UserInforListStyleButtonField) _buttonSet1.getField(2);
			link.setValueText(user.getCreatedAtLong()); //注册时间
			
			link=(UserInforListStyleButtonField) _buttonSet1.getField(3);
			link.setValueText(user.getLocation()); //城市		

			link=(UserInforListStyleButtonField) _buttonSet1.getField(4);
			link.setDescription(user.getUrl().equals("") ? "无" : user.getUrl()); //个人主页		

			link=(UserInforListStyleButtonField) _buttonSet1.getField(5);
			link.setDescription(user.getDescription().equals("") ? "无" : user.getDescription()); //个人简介		
			
			link=(UserInforListStyleButtonField) _buttonSet2.getField(0);
			link.setValueText(user.getStatusesCount()+""); //微博
			link.setChangeListener(null);
			link.setChangeListener(new FieldChangeListener()
			{
				public void fieldChanged(Field field, int context)
				{
					//UiApplication.getUiApplication().pushModalScreen(new ShowUserStatusesScreen(_accessToken, _uid));
					UiApplication.getUiApplication().pushModalScreen(new ShowStatusesScreen(_accessToken, user));
				}
			});
			
			link=(UserInforListStyleButtonField) _buttonSet2.getField(1);
			link.setValueText(user.getFriendsCount()+""); //关注
			link.setChangeListener(null);
			link.setChangeListener(new FieldChangeListener()
			{
				public void fieldChanged(Field field, int context)
				{
					UiApplication.getUiApplication().pushModalScreen(new ShowFriendsScreen(_accessToken, user.getScreenName(), _uid, Config.SCREEN_TYPE_FRIENDS));
				}
			});
			
			link=(UserInforListStyleButtonField) _buttonSet2.getField(2);
			link.setValueText(user.getFollowersCount()+"("+user.getBiFollowersCount()+")"); //粉丝(互粉)
			link.setChangeListener(null);
			link.setChangeListener(new FieldChangeListener()
			{
				public void fieldChanged(Field field, int context)
				{
					UiApplication.getUiApplication().pushModalScreen(new ShowFriendsScreen(_accessToken, user.getScreenName(), _uid, Config.SCREEN_TYPE_FOLLOWERS));
				}
			});
			
			link=(UserInforListStyleButtonField) _buttonSet2.getField(3);
			link.setValueText(user.getFavouritesCount()+""); //收藏
			link.setChangeListener(null);
			link.setChangeListener(new FieldChangeListener()
			{
				public void fieldChanged(Field field, int context)
				{
					UiApplication.getUiApplication().pushModalScreen(new ShowFavoritesScreen(_accessToken, user));
				}
			});
			
			link=(UserInforListStyleButtonField) _buttonSet2.getField(4);
			link.setValueText(user.getTrends().size()+""); //话题
			link.setChangeListener(null);
			link.setChangeListener(new FieldChangeListener()
			{
				public void fieldChanged(Field field, int context)
				{
					Vector trends = user.getTrends();
					StringBuffer sb=new StringBuffer();
					
					sb.append(user.getScreenName()+"关注的话题");
					sb.append("\n\n");
					for(int i=0; i<trends.size(); i++)
					{
						sb.append(i+1+": ");
						sb.append(((UserTrend) trends.elementAt(i)).getHotword());
						sb.append("\n");
					}
					
					Function.errorDialog(sb.toString());
				}
			});
		} catch (Exception e) {}
	}
	
	
	
	
	
	MenuItem menuRefreshInformation=new MenuItem(new StringProvider("刷新信息"), 10, 10)
	{
		public void run()
		{
			User user=null;
			RefreshUserInformationScreen popupScreen=new RefreshUserInformationScreen(_accessToken, _uid);
			UiApplication.getUiApplication().pushModalScreen(popupScreen);
			
			user=popupScreen.getUser();
			if(popupScreen!=null) {popupScreen=null;}
			if(user!=null) {refreshUI(user);}
		}
	};
	
	protected void makeMenu(Menu menu, int instance)
	{
		menu.add(menuRefreshInformation);
		menu.addSeparator();
	};
	
    protected boolean keyChar(char key, int status, int time)
    {
    	if(key==Characters.LATIN_CAPITAL_LETTER_P || key==Characters.LATIN_SMALL_LETTER_P)
    	{
    		Function.errorDialog(Thread.activeCount()-1+"");
    		return true;
    	}
    	
    	return super.keyChar(key, status, time);
    }
}