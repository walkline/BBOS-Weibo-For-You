package weibo;

import org.json.me.JSONObject;

import weibo.model.WeiboException;
import code.WeiboConfig;

public class Account extends Weibo
{
	public Account(String access_token) {setToken(access_token);}

	public JSONObject getUID() throws WeiboException
	{
		return get(WeiboConfig.baseURL + "account/get_uid.json").asJSONObject();
	}
}