package weibo;

import org.json.me.JSONArray;
import org.json.me.JSONException;

import code.WeiboConfig;
import weibo.model.Comment;
import weibo.model.CommentWapper;
import weibo.model.Paging;
import weibo.model.PostParameter;
import weibo.model.WeiboException;

public class Comments extends Weibo
{
	public Comments(String access_token) {setToken(access_token);}
	
	/**
	 * 根据微博ID返回某条微博的评论列表

	 * @param id 需要查询的微博ID
	 * @return list of Comment
	 */
	public CommentWapper getCommentById(String id) throws WeiboException
	{
		return Comment.constructWapperComments(get(WeiboConfig.baseURL + "comments/show.json",
													new PostParameter[] {new PostParameter("id", id)}));
	}

	/**
	 * 根据微博ID返回某条微博的评论列表
	 * @param id 需要查询的微博ID
	 * @param count 单页返回的记录条数，默认为50。
	 * @param page 返回结果的页码，默认为1。
	 * @param filter_by_author 作者筛选类型，0：全部、1：我关注的人、2：陌生人，默认为0。
	 * @return list of Comment
	 */
	public CommentWapper getCommentById(String id, Paging page, Integer filter_by_author) throws WeiboException
	{
		return Comment.constructWapperComments(get(WeiboConfig.baseURL + "comments/show.json",
													new PostParameter[] {
																			new PostParameter("id", id),
																			new PostParameter("filter_by_author", filter_by_author.toString())}, page));
	}

	/**
	 * 获取当前登录用户所发出的评论列表
	 * @return list of Comment
	 */
	public CommentWapper getCommentByMe(int page, int count) throws WeiboException
	{
		return Comment.constructWapperComments(get(WeiboConfig.baseURL + "comments/by_me.json",
													new PostParameter[] {
																			new PostParameter("count", count),
																			new PostParameter("page", page)}));
	}

	/**
	 * 获取当前登录用户所发出的评论列表
	 * 
	 * @param count 单页返回的记录条数，默认为50
	 * @param page 返回结果的页码，默认为1
	 * @param filter_by_source 来源筛选类型，0：全部、1：来自微博的评论、2：来自微群的评论，默认为0
	 * @return list of Comment
	 */
	public CommentWapper getCommentByMe(Paging page, Integer filter_by_source) throws WeiboException
	{
		return Comment.constructWapperComments(get(WeiboConfig.baseURL + "comments/by_me.json",
													new PostParameter[]{new PostParameter("filter_by_author", filter_by_source.toString())}
													, page));
	}

	/**
	 * 获取当前登录用户所接收到的评论列表
	 * @return list of Comment
	 */
	public CommentWapper getCommentToMe(int page, int count) throws WeiboException
	{
		return Comment.constructWapperComments(get(WeiboConfig.baseURL + "comments/to_me.json",
													new PostParameter[] {
																			new PostParameter("count", count),
																			new PostParameter("page", page)}));
	}

	/**
	 * 获取当前登录用户所接收到的评论列表
	 * @param count 单页返回的记录条数，默认为50。
	 * @param page 返回结果的页码，默认为1。
	 * @param filter_by_author 作者筛选类型，0：全部、1：我关注的人、2：陌生人，默认为0。
	 * @param filter_by_source 来源筛选类型，0：全部、1：来自微博的评论、2：来自微群的评论，默认为0。
	 * @return list of Comment
	 */
	public CommentWapper getCommentToMe(Paging page, Integer filter_by_source, Integer filter_by_author) throws WeiboException
	{
		return Comment.constructWapperComments(get(WeiboConfig.baseURL + "comments/to_me.json",
													new PostParameter[] {
																			new PostParameter("filter_by_source", filter_by_source.toString()),
																			new PostParameter("filter_by_author", filter_by_author.toString()) }
													, page));
	}

	/**
	 * 获取当前登录用户的最新评论包括接收到的与发出的
	 * @return list of Comment
	 */
	public CommentWapper getCommentTimeline() throws WeiboException
	{
		return Comment.constructWapperComments(get(WeiboConfig.baseURL + "comments/timeline.json"));
	}

	/**
	 * 获取当前登录用户的最新评论包括接收到的与发出的
	 * @param count 单页返回的记录条数，默认为50。
	 * @param page 返回结果的页码，默认为1。
	 * @return list of Comment
	 */
	public CommentWapper getCommentTimeline(Paging page) throws WeiboException
	{
		return Comment.constructWapperComments(get(WeiboConfig.baseURL + "comments/timeline.json", null, page));
	}

	/**
	 * 获取最新的提到当前登录用户的评论，即@我的评论
	 * @return list of Comment
	 */
	public CommentWapper getCommentMentions(int page, int count) throws WeiboException
	{
		return Comment.constructWapperComments(get(WeiboConfig.baseURL + "comments/mentions.json",
													new PostParameter[] {
																			new PostParameter("count", count),
																			new PostParameter("page", page)}));
	}

	/**
	 * 获取最新的提到当前登录用户的评论，即@我的评论
	 * @param count 单页返回的记录条数，默认为50。
	 * @param page 返回结果的页码，默认为1。
	 * @param filter_by_author 作者筛选类型，0：全部、1：我关注的人、2：陌生人，默认为0。
	 * @param filter_by_source 来源筛选类型，0：全部、1：来自微博的评论、2：来自微群的评论，默认为0。
	 * @return list of Comment
	 */
	public CommentWapper getCommentMentions(Paging page, Integer filter_by_source, Integer filter_by_author) throws WeiboException
	{
		return Comment.constructWapperComments(get(WeiboConfig.baseURL + "comments/mentions.json",
													new PostParameter[] {
																			new PostParameter("filter_by_source", filter_by_source.toString()),
																			new PostParameter("filter_by_author", filter_by_author.toString())}
													, page));
	}

	/**
	 * 根据评论ID批量返回评论信息
	 * @param cids 需要查询的批量评论ID，用半角逗号分隔，最大50
	 * @return list of Comment
	 * @throws JSONException 
	 */
	public JSONArray getCommentShowBatch(String cids) throws WeiboException, JSONException
	{
		return get(WeiboConfig.baseURL + "comments/show_batch.json", new PostParameter[] {new PostParameter("cids", cids) }).asJSONArray();
	}

	/**
	 * 对一条微博进行评论
	 * @param comment 评论内容，必须做URLencode，内容不超过140个汉字
	 * @param id 需要评论的微博ID
	 * @return Comment
	 */
	public Comment createComment(String comment, String id) throws WeiboException
	{
		return new Comment(post(WeiboConfig.baseURL	+ "comments/create.json", new PostParameter[] {
																					new PostParameter("comment", comment),
																					new PostParameter("id", id)}));
	}

	/**
	 * 对一条微博进行评论
	 * @param comment 评论内容，必须做URLencode，内容不超过140个汉字
	 * @param id 需要评论的微博ID
	 * @param comment_ori 当评论转发微博时，是否评论给原微博，0：否、1：是，默认为0。
	 * @return Comment
	 */
	public Comment createComment(String comment, String id, Integer comment_ori) throws WeiboException
	{
		return new Comment(post(WeiboConfig.baseURL + "comments/create.json", new PostParameter[] {
																					new PostParameter("comment", comment),
																					new PostParameter("id", id),
																					new PostParameter("comment_ori", comment_ori.toString())}));
	}

	/**
	 * 回复一条评论 
	 * @param comment 评论内容，必须做URLencode，内容不超过140个汉字
	 * @param cid 需要回复的评论ID
	 * @param id 需要评论的微博ID
	 * @return Comment
	 */
	public Comment replyComment(String cid, String id, String comment) throws WeiboException
	{
		return new Comment(post(WeiboConfig.baseURL + "comments/reply.json", new PostParameter[] {
																					new PostParameter("cid", cid), 
																					new PostParameter("id", id),
																					new PostParameter("comment", comment)}));
	}

	/**
	 * 回复一条评论
	 * @param comment 评论内容，必须做URLencode，内容不超过140个汉字
	 * @param cid 需要回复的评论ID
	 * @param id 需要评论的微博ID
	 * @param without_mention 回复中是否自动加入“回复@用户名”，0：是、1：否，默认为0。
	 * @param comment_ori 当评论转发微博时，是否评论给原微博，0：否、1：是，默认为0。
	 * @return Comment
	 */
	public Comment replyComment(String cid, String id, String comment, Integer without_mention, Integer comment_ori) throws WeiboException
	{
		return new Comment(post(WeiboConfig.baseURL + "comments/reply.json", new PostParameter[] {
																					new PostParameter("comment", comment),
																					new PostParameter("id", id),
																					new PostParameter("cid", cid),
																					new PostParameter("without_mention",without_mention.toString()),
																					new PostParameter("comment_ori", comment_ori.toString())}));
	}

	/**
	 * 删除一条评论
	 * @param cid 要删除的评论ID，只能删除登录用户自己发布的评论
	 * @return Comment 
	 */
	public Comment destroyComment(String cid) throws WeiboException
	{
		return new Comment(post(WeiboConfig.baseURL + "comments/destroy.json", new PostParameter[]{new PostParameter("cid", cid)}));
	}

	/**
	 * 根据评论ID批量删除评论
	 * @param ids 需要删除的评论ID，用半角逗号隔开，最多20个
	 * @return list of Comment
	 * @throws JSONException 
	 */
	public JSONArray destoryCommentBatch(String cids) throws WeiboException, JSONException
	{
		return post(WeiboConfig.baseURL + "comments/destroy_batch.json", new PostParameter[]{new PostParameter("cids", cids)}).asJSONArray();
	}
}