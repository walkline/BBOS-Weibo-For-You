package weibo;

import java.util.Vector;

import code.WeiboConfig;

import weibo.model.Favorites;
import weibo.model.Paging;
import weibo.model.WeiboException;

public class Favorite extends Weibo
{
	public Favorite(String access_token) {setToken(access_token);}
	
	/*----------------------------收藏接口----------------------------------------*/
	/**
	 * 获取当前登录用户的收藏列表
	 * @return list of the Status
	 */
	public Vector getFavorites() throws WeiboException
	{
		return Favorites.constructFavorites(get(WeiboConfig.baseURL + "favorites.json"));
	}

	/**
	 * 获取当前登录用户的收藏列表
	 * @param page、count
	 * @return list of the Status
	 */
	public Vector getFavorites(Paging page) throws WeiboException
	{
		return Favorites.constructFavorites(get(WeiboConfig.baseURL + "favorites.json",	null, page));
	}
}