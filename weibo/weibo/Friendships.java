package weibo;

import code.WeiboConfig;
import weibo.model.PostParameter;
import weibo.model.User;
import weibo.model.UserWapper;
import weibo.model.WeiboException;

public class Friendships extends Weibo
{

	public Friendships(String access_token) {setToken(access_token);}
	
	/*----------------------------关系接口----------------------------------------*/
	/**
	 * 获取用户的关注列表
	 * @return list of the user's follow
	 */
	public UserWapper getFriendsByID(String id) throws WeiboException
	{
		return User.constructWapperUsers(get(WeiboConfig.baseURL + "friendships/friends.json",
											new PostParameter[] {
																	new PostParameter("uid", id), 
																	new PostParameter("trim_status", 0),
																	new PostParameter("count", 20)
																}));
	}

	/**
	 * 获取用户的关注列表
	 * @return list of the user's follow
	 */
	public UserWapper getFriendsByScreenName(String screen_name) throws WeiboException
	{
		return User.constructWapperUsers(get(WeiboConfig.baseURL + "friendships/friends.json",
											new PostParameter[] {new PostParameter("screen_name", screen_name)}));
	}
	
	/**
	 * 获取用户的粉丝列表
	 * @param screen_name 需要查询的用户昵称
	 * @return list of users
	 */
	public UserWapper getFollowersByName(String screen_name) throws WeiboException
	{
		return User.constructWapperUsers(get(WeiboConfig.baseURL + "friendships/followers.json",
											new PostParameter[] {new PostParameter("screen_name", screen_name) }));
	}

	/**
	 * 获取用户的粉丝列表
	 * @param screen_name 需要查询的用户昵称
	 * @param count 单页返回的记录条数，默认为500，最大不超过5000
	 * @param cursor 返回结果的游标，下一页用返回值里的next_cursor，上一页用previous_cursor，默认为0
	 * @return list of users
	 */
	public UserWapper getFollowersByName(String screen_name, Integer count, Integer cursor) throws WeiboException
	{
		return User.constructWapperUsers(get(WeiboConfig.baseURL + "friendships/followers.json",
											new PostParameter[] {
																	new PostParameter("screen_name", screen_name),
																	new PostParameter("count", count.toString()),
																	new PostParameter("cursor", cursor.toString())
																}));
	}

	/**
	 * 获取用户的粉丝列表
	 * @param uid 需要查询的用户ID
	 * @return list of users
	 */
	public UserWapper getFollowersById(String uid) throws WeiboException
	{
		return User.constructWapperUsers(get(WeiboConfig.baseURL + "friendships/followers.json",
											new PostParameter[] {
																	new PostParameter("uid", uid),
																	new PostParameter("trim_status", 0),
																	new PostParameter("count", 20)}));
	}

	/**
	 * 获取用户的粉丝列表
	 * @param screen_name 需要查询的用户昵称
	 * @param count 单页返回的记录条数，默认为500，最大不超过5000
	 * @param cursor 返回结果的游标，下一页用返回值里的next_cursor，上一页用previous_cursor，默认为0
	 * @return list of users
	 */
	public UserWapper getFollowersById(String uid, Integer count, Integer cursor) throws WeiboException
	{
		return User.constructWapperUsers(get(WeiboConfig.baseURL + "friendships/followers.json",
											new PostParameter[] {
																	new PostParameter("uid", uid),
																	new PostParameter("count", count.toString()),
																	new PostParameter("cursor", cursor.toString())
																}));
	}
}