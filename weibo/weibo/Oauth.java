package weibo;

import org.json.me.JSONObject;

import weibo.http.AccessToken;
import weibo.model.PostParameter;
import weibo.model.WeiboException;
import code.Function;
import code.WeiboConfig;

public class Oauth extends Weibo
{
	// ----------------------------针对站内应用处理SignedRequest获取accesstoken----------------------------------------
	public String access_token;
	public String user_id;

	public String getToken() {return access_token;}

	public Oauth() {}
	public Oauth(String accessToken)
	{
		access_token=accessToken;
		setToken(access_token);
	}
	
/*	
	/*
	 * 处理解析后的json解析
	 
	public String ts(String json)
	{
		try {
			JSONObject jsonObject = new JSONObject(json);
			access_token = jsonObject.getString("oauth_token");
			user_id = jsonObject.getString("user_id");
		} catch (JSONException e) {}
		return access_token;

	}
*/
	/*----------------------------Oauth接口--------------------------------------*/

	public AccessToken getAccessTokenByCode(String code) throws WeiboException
	{
		return new AccessToken(post(WeiboConfig.accessTokenURL,
													new PostParameter[] {
															new PostParameter("client_id", WeiboConfig.client_ID),
															new PostParameter("client_secret", WeiboConfig.client_SERCRET),
															new PostParameter("grant_type", "authorization_code"),
															new PostParameter("code", code),
															new PostParameter("redirect_uri", WeiboConfig.redirect_URI)}));
	}

	public String authorize(String response_type,String state) throws WeiboException
	{
		return WeiboConfig.authorizeURL + "?client_id="	+ WeiboConfig.client_ID + 
										  "&redirect_uri=" + WeiboConfig.redirect_URI + 
										  "&response_type=" + response_type +
										  "&display=mobile";
	}
	
	public String authorize(String response_type,String state,String scope) throws WeiboException
	{
		return WeiboConfig.authorizeURL + "?client_id=" + WeiboConfig.client_ID + 
										  "&redirect_uri=" + WeiboConfig.redirect_URI + 
										  "&response_type=" + response_type	+ 
										  //"&state="+state +
										  "&scope="+scope;
	}
	
	public JSONObject revoke() throws WeiboException
	{
		return get(WeiboConfig.revokeURL, new PostParameter[] {new PostParameter("access_token", access_token)}).asJSONObject();
	}
	
	public JSONObject validateToken() throws WeiboException
	{
		return post(WeiboConfig.getTokenURL, new PostParameter[] {new PostParameter("access_token", access_token)}).asJSONObject();	
	}
}