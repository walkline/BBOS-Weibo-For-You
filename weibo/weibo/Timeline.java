package weibo;

import weibo.model.Paging;
import weibo.model.PostParameter;
import weibo.model.Status;
import weibo.model.StatusWapper;
import weibo.model.WeiboException;
import weibo.http.ImageItem;
import code.WeiboConfig;

public class Timeline extends Weibo
{

	public Timeline(String access_token) {setToken(access_token);}

	/**
	 * 获取当前登录用户最新发表的微博列表
	 */
	public StatusWapper getUserTimeline() throws WeiboException
	{
		return Status.constructWapperStatus(get(WeiboConfig.baseURL + "statuses/user_timeline.json"));
	}
	
	/**
	 * 获取指定ID用户最新发表的微博列表
	 */
	public StatusWapper getUserTimelineByUid(String uid) throws WeiboException
	{
		return Status.constructWapperStatus(get(WeiboConfig.baseURL + "statuses/user_timeline.json",
												new PostParameter[]{new PostParameter("uid", uid)}));
	}

	public StatusWapper getUserTimelineByUid(String uid, int page, int count) throws WeiboException
	{
		return Status.constructWapperStatus(get(WeiboConfig.baseURL + "statuses/user_timeline.json",
												new PostParameter[]{
																		new PostParameter("uid", uid),
																		new PostParameter("page", page),
																		new PostParameter("count", count)})
												);
	}

	/**
	 * 获取指定屏幕名称用户最新发表的微博列表
	 */
	public StatusWapper getUserTimelineByName(String screen_name) throws WeiboException
	{
		return Status.constructWapperStatus(get(WeiboConfig.baseURL + "statuses/user_timeline.json",
												new PostParameter[]{new PostParameter("screen_name", screen_name)}));
	}
	
	/**
	 * 获取某个用户最新发表的微博列表
	 * 
	 * @param uid 需要查询的用户ID。
	 * @param screen_name 需要查询的用户昵称。
	 * @param count 单页返回的记录条数，默认为50。
	 * @param page 返回结果的页码，默认为1。
	 * @param base_app 是否只获取当前应用的数据。0为否（所有数据），1为是（仅当前应用），默认为0。
	 * @param feature 过滤类型ID，0：全部、1：原创、2：图片、3：视频、4：音乐，默认为0。
	 * @return list of the user_timeline
	 * @throws WeiboException when Weibo service or network is unavailable
	 */
	public StatusWapper getUserTimelineByUid(String uid, Paging page, int base_app, int feature) throws WeiboException
	{
		return Status.constructWapperStatus(get(WeiboConfig.baseURL + "statuses/user_timeline.json",
												new PostParameter[] {
																		new PostParameter("uid", uid),
																		new PostParameter("base_app", Integer.toString(base_app)),
																		new PostParameter("feature", Integer.toString(feature))},
												page));
	}
	public StatusWapper getUserTimelineByName(String screen_name, Paging page,Integer base_app, Integer feature) throws WeiboException
	{
		return Status.constructWapperStatus(get(WeiboConfig.baseURL + "statuses/user_timeline.json",
												new PostParameter[] {
																		new PostParameter("screen_name", screen_name),
																		new PostParameter("base_app", base_app.toString()),
																		new PostParameter("feature", feature.toString()) },
												page));
	}
	
	/**
	 * 返回最新的公共微博
	 * @return list of statuses of the Public Timeline
	 */
	public StatusWapper getPublicTimeline() throws WeiboException
	{
		return Status.constructWapperStatus(get(WeiboConfig.baseURL + "statuses/public_timeline.json"));
	}

	/**
	 * 返回最新的公共微博
	 * @param count 单页返回的记录条数，默认为20。
	 * @param baseApp 是否仅获取当前应用发布的信息。0为所有，1为仅本应用。
	 */
	public StatusWapper getPublicTimeline(int count) throws WeiboException
	{
		return Status.constructWapperStatus(get(WeiboConfig.baseURL + "statuses/public_timeline.json",
												new PostParameter[] {
																		new PostParameter("count", count)}));
	}
	
	/**
	 * 获取当前登录用户及其所关注用户的最新20条微博消息。
	 * 和用户登录 http://weibo.com 后在“我的首页”中看到的内容相同。
	 * @return list of the Friends Timeline
	 */
	public StatusWapper getFriendsTimeline() throws WeiboException
	{
		return Status.constructWapperStatus(get(WeiboConfig.baseURL + "statuses/friends_timeline.json"));
	}

	public StatusWapper getFriendsTimeline(int page, int count) throws WeiboException
	{
		return Status.constructWapperStatus(get(WeiboConfig.baseURL + "statuses/friends_timeline.json",
												new PostParameter[]{
																		new PostParameter("page", page),
																		new PostParameter("count", count)})
												);
	}

	/**
	 * 获取当前登录用户及其所关注用户的最新微博消息。<br/>
	 * 和用户登录 http://weibo.com 后在“我的首页”中看到的内容相同。
	 * @param paging 相关分页参数
	 * @param feature 过滤类型ID，0：全部、1：原创、2：图片、3：视频、4：音乐，默认为0。
	 */
	public StatusWapper getFriendsTimeline(Integer baseAPP, Integer feature, Paging paging) throws WeiboException
	{
		return Status.constructWapperStatus(get(WeiboConfig.baseURL + "statuses/friends_timeline.json",
												new PostParameter[] {
																		new PostParameter("base_app", baseAPP.toString()),
																		new PostParameter("feature", feature.toString()) },
												paging));
	}
	
	/**
	 * 发布一条新微博
	 * @param status 要发布的微博文本内容，必须做URLencode，内容不超过140个汉字
	 * @return Status
	 */
	public Status UpdateStatus(String status) throws WeiboException
	{
		return new Status(post(WeiboConfig.baseURL + "statuses/update.json",
									new PostParameter[] {new PostParameter("status", status)}, true));
	}
	
	/**
	 * 上传图片并发布一条新微博
	 * @param status 要发布的微博文本内容，必须做URLencode，内容不超过140个汉字
	 * @param pic 要上传的图片，仅支持JPEG、GIF、PNG格式，图片大小小于5M。
	 * @return Status
	 */
	public Status UploadStatus(String status, ImageItem item) throws WeiboException
	{
		return new Status(multPartURL(WeiboConfig.baseURL + "statuses/upload.json",
											new PostParameter[] {new PostParameter("status", status)}, item));
	}
}