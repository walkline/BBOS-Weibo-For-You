package weibo;

import java.util.Vector;

import code.WeiboConfig;

import weibo.model.Paging;
import weibo.model.PostParameter;
import weibo.model.UserTrend;
import weibo.model.WeiboException;

public class Trend extends Weibo
{

	public Trend(String access_token) {setToken(access_token);}

	/*----------------------------话题接口----------------------------------------*/
	/**
	 * 获取某人的话题列表
	 * 
	 * @param uid
	 *            需要获取话题的用户的UID
	 * @return list of the userTrend
	 * @throws WeiboException
	 *             when Weibo service or network is unavailable
	 * @version weibo4j-V2 1.0.1
	 * @see <a href="http://open.weibo.com/wiki/2/trends">trends</a>
	 * @since JDK 1.5
	 */
	public Vector getTrends(String uid) throws WeiboException
	{
		return UserTrend.constructTrendList(get(WeiboConfig.baseURL + "trends.json",
												new PostParameter[] {new PostParameter("uid", uid)}));
	}

	/**
	 * 获取某人的话题列表
	 * 
	 * @param uid
	 *            需要获取话题的用户的UID
	 * @param page
	 *            返回结果的页码，默认为1
	 * @return list of the userTrend
	 * @throws WeiboException
	 *             when Weibo service or network is unavailable
	 * @version weibo4j-V2 1.0.1
	 * @see <a href="http://open.weibo.com/wiki/2/trends">trends</a>
	 * @since JDK 1.5
	 */
	public Vector getTrends(String uid, Paging page) throws WeiboException 
	{
		return UserTrend.constructTrendList(get(WeiboConfig.baseURL + "trends.json",
												new PostParameter[] { new PostParameter("uid", uid) }, page));
	}
}
