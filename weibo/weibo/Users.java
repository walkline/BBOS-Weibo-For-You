package weibo;

import weibo.model.PostParameter;
import weibo.model.User;
import weibo.model.WeiboException;
import code.WeiboConfig;

public class Users extends Weibo
{
	public Users(String access_token) {setToken(access_token);}
	
	public User showUserById(String uid) throws WeiboException
	{
		return new User(get(WeiboConfig.baseURL + "users/show.json", new PostParameter[] {new PostParameter("uid", uid)}).asJSONObject());
	}
}