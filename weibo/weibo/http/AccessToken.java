package weibo.http;

import java.util.Vector;

import org.json.me.JSONException;
import org.json.me.JSONObject;

import code.Function;

import weibo.Oauth;
import weibo.model.JSONError;
import weibo.model.StatusWapper;
import weibo.model.WeiboException;
import weibo.model.WeiboResponse;

public class AccessToken extends WeiboResponse
{
	private String appKey;
	private String accessToken;
	private String uid;
	private long expiresTime = 0;
	private long expireIn;	
	
	public AccessToken() {}
	
	public AccessToken(Response res) throws WeiboException
	{
		super(res);
		
		try{
			JSONObject json =res.asJSONObject();
			setToken(json.optString("access_token"));
			setExpiresIn(json.optLong("expires_in"));
			setUid(json.getString("uid"));
		} catch (JSONException je) {throw new WeiboException(je.toString());}
	}
	
/*	
	AccessToken(String res) throws WeiboException,JSONException
	{
		super();
		
		JSONObject json =new JSONObject(res);
		
		accessToken = json.getString("access_token");
		expireIn = json.getString("expires_in");
		uid = json.getString("uid");
	}
*/
	public boolean validateToken() 
	{
		Oauth oauth=new Oauth(accessToken);
		JSONObject object;
		try {
			object = oauth.validateToken();
			
			JSONError error=new JSONError(object);
			if(error.getErrorCode()!=0) {}//Function.errorDialog(error.toString());}

			appKey=object.optString("appkey");
			expireIn=object.optLong("expire_in");
		} catch (WeiboException e)
		{
			Function.errorDialog("检查用户授权时出错，请检查你的网络环境并确保手机可以连接到网络！\n\n" + e.toString());
			System.exit(0);
		}
		
		return (appKey!=null && expireIn!=0);
	}
	
	public boolean isSessionValid()
	{
		return (accessToken!=null && (expiresTime == 0 || (System.currentTimeMillis() < expiresTime)));
	}

	private void setExpiresIn(long expiresIn)
	{
		if(expiresIn!=0) {setExpiresTime(System.currentTimeMillis() + expiresIn * 1000);}
	}

	public void setExpiresTime(long mExpiresTime) {expiresTime = mExpiresTime;}
	public void setToken(String mToken) {accessToken = mToken;}
	public void setUid(String mUid){uid=mUid;} 
	
	public String getToken() {return accessToken;}
	//public String getExpireIn() {return expireIn;}
	public long getExpiresTime() {return expiresTime;}
	public String getUID() {return uid;}

	public int hashCode()
	{
		final int prime = 31;
		int result = 1;
		
		result = prime * result + ((accessToken == null) ? 0 : accessToken.hashCode());
		//result = prime * result + ((expireIn == 0) ? 0 : expireIn.hashCode());
		result = prime * result;

		return result;
	}

	public boolean equals(Object obj)
	{
		if(this == obj) {return true;}
		if(obj == null) {return false;}
		if(getClass()!= obj.getClass()) {return false;}
		
		AccessToken other = (AccessToken) obj;
		if(accessToken == null)
		{
			if(other.accessToken!= null) {return false;}
		} else if(!accessToken.equals(other.accessToken)) {return false;}
		
		if(expireIn == 0)
		{
			if(other.expireIn!= 0) {return false;}
		} else if(expireIn!=other.expireIn) {return false;}
		//if (refreshToken == null) {
		//	if (other.refreshToken != null)
		//		return false;
		//} else if (!refreshToken.equals(other.refreshToken))
		//	return false;
		return true;
	}

	public String toString()
	{
		return "{\"access_token\":\"" + accessToken + "\", \"expires_in\":" + expireIn + ", \"uid\":\"" + uid+"\"}";
	}
}