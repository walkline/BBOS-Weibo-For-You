package weibo.model;

import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;
import java.util.Vector;

import net.rim.device.api.i18n.SimpleDateFormat;

import org.json.me.JSONArray;
import org.json.me.JSONException;
import org.json.me.JSONObject;

import weibo.http.Response;

public class Comment extends WeiboResponse
{
	private Date createdAt;                    //评论时间
	private long id;                           //评论id
	private String mid;						   //评论id
	private String idstr;					   //评论id
	private String text;                       //评论内容
	private Source source;                     //内容来源
	private Comment replycomment = null;       //回复的评论内容
	private User user = null;                  //User对象
	private Status status = null;              //Status对象

	public Comment(Response res) throws WeiboException
	{
		super(res);
		
		JSONObject json =res.asJSONObject();
		
		try {
			id = json.getLong("id");
			mid = json.getString("mid");
			idstr = json.getString("idstr");
			text = json.getString("text");
			//source = json.getString("source");
			createdAt = parseDate(json.getString("created_at"));//, "EEE MMM dd HH:mm:ss z yyyy");
			if(!json.isNull("source")){source = new Source(json.getString("source"));}
			if(!json.isNull("user")) {user = new User(json.getJSONObject("user"));}
			if(!json.isNull("status")) {status = new Status(json.getJSONObject("status"));}
			if(!json.isNull("reply_comment")) {replycomment = (new Comment(json.getJSONObject("reply_comment")));}
		} catch (JSONException je) {throw new WeiboException(je.getMessage() + ":" + json.toString());}
	}

	public Comment(JSONObject json)throws WeiboException, JSONException
	{
		id = json.getLong("id");
		mid = json.getString("mid");
		idstr = json.getString("idstr");
		text = json.getString("text");
		//source = json.getString("source");
		createdAt = parseDate(json.getString("created_at"));//, "EEE MMM dd HH:mm:ss z yyyy");
		if(!json.isNull("source")){source = new Source(json.getString("source"));}
		if(!json.isNull("user")) {user = new User(json.getJSONObject("user"));}
		if(!json.isNull("status")) {status = new Status(json.getJSONObject("status"));}	
		if(!json.isNull("reply_comment")) {replycomment = (new Comment(json.getJSONObject("reply_comment")));}
	}

	public Comment(String str) throws WeiboException, JSONException
	{
		super();
		
		JSONObject json = new JSONObject(str);
		
		id = json.getLong("id");
		mid = json.getString("mid");
		idstr = json.getString("idstr");
		text = json.getString("text");
		//source = json.getString("source");
		createdAt = parseDate(json.getString("created_at"));//, "EEE MMM dd HH:mm:ss z yyyy");
		if(!json.isNull("source")){source = new Source(json.getString("source"));}
		if(!json.isNull("user")) {user = new User(json.getJSONObject("user"));}
		if(!json.isNull("status")) {status = new Status(json.getJSONObject("status"));}	
		if(!json.isNull("reply_comment")) {replycomment = (new Comment(json.getJSONObject("reply_comment")));}
	}

	public static CommentWapper constructWapperComments(Response res) throws WeiboException
	{
		JSONObject json = res.asJSONObject();
		
		try {
			JSONArray comments = json.getJSONArray("comments");
			int size = comments.length();
			Vector comment = new Vector(size);
			
			for (int i = 0; i < size; i++) {comment.addElement(new Comment(comments.getJSONObject(i)));}
			
			//long previousCursor = json.getLong("previous_curosr");
			//long nextCursor = json.getLong("next_cursor");
			//long totalNumber = json.getLong("total_number");
			//String hasvisible = json.getString("hasvisible");
			//return new CommentWapper(comment, previousCursor, nextCursor,totalNumber,hasvisible);
			return new CommentWapper(comment);
		} catch (JSONException jsone) {throw new WeiboException(jsone.toString());
		}
	}
	
	public String getCreatedAtLong()
	{
		SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd HH:mm");
		
		return sdf.formatLocal(createdAt.getTime());
	}

	public String getCreatedAtShort()
	{
		String formatString="MM/dd HH:mm";
		Calendar calendarToday=Calendar.getInstance(TimeZone.getDefault());
		Calendar calendarCreate=Calendar.getInstance(TimeZone.getDefault());
		
		calendarToday.setTime(new Date(System.currentTimeMillis()));
		calendarCreate.setTime(createdAt);
		
		calendarToday.set(Calendar.HOUR_OF_DAY, 0);
		calendarCreate.set(Calendar.HOUR_OF_DAY, 0);
		calendarToday.set(Calendar.MINUTE, 0);
		calendarCreate.set(Calendar.MINUTE, 0);
		calendarToday.set(Calendar.SECOND, 0);
		calendarCreate.set(Calendar.SECOND, 0);
		calendarToday.set(Calendar.MILLISECOND, 1);
		calendarCreate.set(Calendar.MILLISECOND, 1);
		
		int todayYear=calendarToday.get(Calendar.YEAR);
		int createYear=calendarCreate.get(Calendar.YEAR);
		int todayMonth=calendarToday.get(Calendar.MONTH);
		int createMonth=calendarCreate.get(Calendar.MONTH);
		int todayDay=calendarToday.get(Calendar.DAY_OF_MONTH);
		int createDay=calendarCreate.get(Calendar.DAY_OF_MONTH);

		if((todayYear-createYear)>=1)
		{
			formatString="yyyy/MM/dd HH:mm";
		} else if((todayMonth==createMonth) && todayDay==createDay)
		{
			formatString="HH:mm";
		}
		
		SimpleDateFormat sdf=new SimpleDateFormat(formatString);
		
		return sdf.formatLocal(createdAt.getTime());
	}
	
	//public Date getCreatedAt() {return createdAt;}
	public long getId() {return id;}
	public String getText() {return text;}
	public Source getSource() {return source;}
	public Comment getReplycomment() {return replycomment;}
	public User getUser() {return user;}
	public Status getStatus() {return status;}
	public String getMid() {return mid;}
	public String getIdstr() {return idstr;}

	public void setMid(String mid) {this.mid = mid;}
	public void setIdstr(String idstr) {this.idstr = idstr;}
	public void setCreatedAt(Date createdAt) {this.createdAt = createdAt;}
	public void setId(long id) {this.id = id;}
	public void setText(String text) {this.text = text;}
	public void setSource(Source source) {this.source = source;}
	public void setReplycomment(Comment replycomment) {this.replycomment = replycomment;}
	public void setUser(User user) {this.user = user;}
	public void setStatus(Status status) {this.status = status;}

	public int hashCode()
	{
		final int prime = 31;
		int result = 1;
		
		result = prime * result + (int) (id ^ (id >>> 32));
		
		return result;
	}

	public boolean equals(Object obj)
	{
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Comment other = (Comment) obj;
		if (id != other.id)
			return false;
		return true;
	}

	public String toString()
	{
		return "Comment [createdAt=" + createdAt + ", id=" + id + ", mid="
				+ mid + ", idstr=" + idstr + ", text=" + text + ", source="
				+ source + ", replycomment=" + replycomment + ", user=" + user
				+ ", status=" + status +"]";
	}
}