package weibo.model;

import java.util.Vector;

public class CommentWapper
{
	private Vector comments;
	private long previousCursor;
	private long nextCursor;
	private long totalNumber;
	private String hasvisible;
	
	public CommentWapper(Vector comments) {this.comments=comments;}
	
	public CommentWapper(Vector comments, long previousCursor, long nextCursor, long totalNumber,String hasvisible)
	{
		this.comments = comments;
		this.previousCursor = previousCursor;
		this.nextCursor = nextCursor;
		this.totalNumber = totalNumber;
		this.hasvisible = hasvisible;
	}

	public Vector getComments() {return comments;}
	//public long getPreviousCursor() {return previousCursor;}
	//public long getNextCursor() {return nextCursor;}
	//public long getTotalNumber() {return totalNumber;}
	//public String getHasvisible() {return hasvisible;}
	
	public void setComments(Vector comments) {this.comments = comments;}
	//public void setPreviousCursor(long previousCursor) {this.previousCursor = previousCursor;}
	//public void setNextCursor(long nextCursor) {this.nextCursor = nextCursor;}
	//public void setTotalNumber(long totalNumber) {this.totalNumber = totalNumber;}
	//public void setHasvisible(String hasvisible) {this.hasvisible = hasvisible;}
}