package weibo.model;

import java.util.Date;
import java.util.Vector;

import org.json.me.JSONArray;
import org.json.me.JSONException;
import org.json.me.JSONObject;

import weibo.http.Response;

public class Favorites extends WeiboResponse
{
	private Date favoritedTime;                        //添加收藏的时间
	private Status status;                             //收藏的status
	private Vector tags;                   //收藏的tags
	private static int totalNumber;
	public Favorites(Response res) throws WeiboException{
		super(res);
		JSONObject json = null;
		try {
			json = res.asJSONObject();
			favoritedTime = parseDate(json.getString("favorited_time"));
			if(!json.isNull("status")){
				status = new Status(json.getJSONObject("status"));
			}
			if(!json.isNull("tags")){
				JSONArray list = json.getJSONArray("tags");
				int size = list.length();
				Vector tag = new Vector(size);
				for (int i = 0;i< size;i++){
					tag.addElement(new FavoritesTag(list.getJSONObject(i)));
				}
			}
		} catch (JSONException je) {throw new WeiboException(je.getMessage() + ":" + json.toString());}
	} 
	
	Favorites(JSONObject json) throws WeiboException, JSONException{
		favoritedTime = parseDate(json.getString("favorited_time"));
		if(!json.isNull("status")){
			status = new Status(json.getJSONObject("status"));
		}
		if(!json.isNull("tags")){
			JSONArray list = json.getJSONArray("tags");
			int size = list.length();
			tags = new Vector(size);
			for (int i = 0;i< size;i++){
				tags.addElement(new FavoritesTag(list.getJSONObject(i)));
			}
		}

	}
	public static Vector constructFavorites(Response res) throws WeiboException{
		try {
			JSONArray list = res.asJSONObject().getJSONArray("favorites");
			int size = list.length();
			Vector favorites = new Vector(size);
			for (int i = 0; i < size; i++) {
				favorites.addElement(new Favorites(list.getJSONObject(i)));
			}
			
			totalNumber =res.asJSONObject().getInt("total_number");
			
			return favorites;
		} catch (JSONException jsone) {throw new WeiboException(jsone.toString());} 
	}
	
	public Status getStatus() {return status;}
	public Vector getTags() {return tags;}
	public Date getFavoritedTime() {return favoritedTime;}	

	public void setStatus(Status status) {this.status = status;}
	public void setTags(Vector tags) {this.tags = tags;}
	public void setFavoritedTime(Date favoritedTime) {this.favoritedTime = favoritedTime;}
	
	public int hashCode()
	{
		final int prime = 31;
		int result = 1;
		result = prime * result	+ ((favoritedTime == null) ? 0 : favoritedTime.hashCode());
		
		return result;
	}
	
	public boolean equals(Object obj) 
	{
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Favorites other = (Favorites) obj;
		if (favoritedTime == null) {
			if (other.favoritedTime != null)
				return false;
		} else if (!favoritedTime.equals(other.favoritedTime))
			return false;
		return true;
	}
	

	public String toString()
	{
		return "Favorites [" +
				"favorited_time=" + favoritedTime + 
				", status=" + status.toString() + 
				", FavoritesTag=" + ((tags==null)?"null":tags.toString()) + 
				", total_number = "+totalNumber+
				"]";
	}
}