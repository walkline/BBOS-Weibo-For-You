package weibo.model;

import java.util.Vector;

import org.json.me.JSONArray;
import org.json.me.JSONException;
import org.json.me.JSONObject;

import weibo.http.Response;

public class FavoritesTag extends WeiboResponse
{
	private String id;           //标签id
	private String tag;          //标签
	private int count;           //该标签下收藏的微博数

	public FavoritesTag(JSONObject json) throws WeiboException,JSONException
	{
		id = json.getString("id");
		tag = json.getString("tag");
		if(!json.isNull("count")) {count = json.getInt("count");}
	}

	public static Vector constructTags(Response res) throws WeiboException
	{
		try {
			JSONArray list = res.asJSONArray();
			int size = list.length();
			Vector tags = new Vector(size);
			for (int i = 0; i < size; i++) {
				tags.addElement(new FavoritesTag(list.getJSONObject(i)));
			}
			
			return tags;
		} catch (JSONException jsone) {throw new WeiboException(jsone.toString());}
		  catch (WeiboException te) {throw te;}
	}
	
	public static Vector constructTag(Response res) throws WeiboException
	{
		try {
			JSONArray list = res.asJSONObject().getJSONArray("tags");
			int size = list.length();
			Vector tags = new Vector(size);
			for (int i = 0; i < size; i++) {
				tags.addElement(new FavoritesTag(list.getJSONObject(i)));
			}
			
			return tags;
		} catch (JSONException jsone) {throw new WeiboException(jsone.toString());}
		  catch (WeiboException te) {throw te;}
	}

	public int hashCode()
	{
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		
		return result;
	}

	public boolean equals(Object obj) 
	{
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		FavoritesTag other = (FavoritesTag) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	public String getId() {return id;}
	public String getTag() {return tag;}
	public int getCount() {return count;}

	public void setId(String id) {this.id = id;}
	public void setTag(String tag) {this.tag = tag;}
	public void setCount(int count) {this.count = count;}

	public String toString()
	{
		return "FavoritesTag [id=" + id + ", tag=" + tag + ", count=" + count
		+ "]";
	}
}