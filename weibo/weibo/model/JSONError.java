package weibo.model;

import org.json.me.JSONException;
import org.json.me.JSONObject;

public class JSONError
{
	private String error;
	private String request;
	private long error_code;

	public JSONError(String responsetext)
	{
		if(responsetext!= null)
		{
			if (responsetext.indexOf("{") >= 0)
			{
				try
				{
					JSONObject json = new JSONObject(responsetext);
					error=json.getString("error");
					error_code=json.getLong("error_code");
					request=json.getString("request");
				} catch (JSONException e) {}
			}
		}
	}

	public JSONError(JSONObject jsonObject)
	{
		if(jsonObject!=null)
		{
			try
			{
				error=jsonObject.getString("error");
				error_code=jsonObject.getLong("error_code");
				request=jsonObject.getString("request");
			} catch (JSONException e) {}
		}
	}
	
	public long getErrorCode() {return error_code;}
	
	public String toString()
	{
		return "JSON Exception\n\n\u2022 Error: " + error + "\n\u2022 Error Code: " + error_code + "\n\u2022 Request: " + request;
	}
}