package weibo.model;

import org.json.me.JSONArray;
import org.json.me.JSONException;
import org.json.me.JSONObject;

import code.StringUtility;

public class PictureUrls
{
	private String[] thumbnail_urls=null;
	private String[] bmiddle_urls=null;
	private String[] original_urls=null;
	
	public PictureUrls(JSONArray json) throws JSONException
	{
		if(json.toString().equals("[]")) {return;}
		
		JSONObject object;

		thumbnail_urls=new String[json.length()];
		bmiddle_urls=new String[json.length()];
		original_urls=new String[json.length()];
		
		for(int i=0; i<json.length(); i++)
		{
			object=json.getJSONObject(i);
			thumbnail_urls[i]=object.getString("thumbnail_pic");
			
			bmiddle_urls[i]=StringUtility.replace(thumbnail_urls[i], "thumbnail", "bmiddle");
			original_urls[i]=StringUtility.replace(thumbnail_urls[i], "thumbail", "large");
		}
	}
	
	public String[] getThumbnailPics() {return thumbnail_urls;}
	public String[] getMiddlePics() {return bmiddle_urls;}		
	public String[] getOriginalPics() {return original_urls;}		
}