
package weibo.model;

import javax.microedition.io.file.FileConnection;

import net.rim.device.api.system.Characters;

public class PostParameter
{
    String name;
    String value;
    private FileConnection file = null;

    public PostParameter(String name, String value) {
        this.name = name;
        this.value = value;
    }

    public PostParameter(String name, double value) {
        this.name = name;
        this.value = String.valueOf(value);
    }

    public PostParameter(String name, int value) {
        this.name = name;
        this.value = String.valueOf(value);
    }

    public PostParameter(String name, long value) {
        this.name = name;
        this.value = String.valueOf(value);
    }

    public PostParameter(String name, FileConnection file) {
        this.name = name;
        this.file = file;
    }
    
    public String getName(){return name;}
    public String getValue(){return value;}
    public FileConnection getFile() {return file;}

    public boolean isFile(){return null != file;}
    
    private static final String JPEG = "image/jpeg";
    private static final String GIF = "image/gif";
    private static final String PNG = "image/png";
    private static final String OCTET = "application/octet-stream";

    public String getContentType() {
        if (!isFile()) {
            throw new IllegalStateException("not a file");
        }
        String contentType;
        String extensions = file.getName();
        int index = extensions.lastIndexOf(Characters.FULL_STOP);
        if (-1 == index) {
            // no extension
            contentType = OCTET;
        } else {
            extensions = extensions.substring(extensions.lastIndexOf(Characters.FULL_STOP) + 1).toLowerCase();
            if (extensions.length() == 3) {
                if ("gif".equals(extensions)) {
                    contentType = GIF;
                } else if ("png".equals(extensions)) {
                    contentType = PNG;
                } else if ("jpg".equals(extensions)) {
                    contentType = JPEG;
                } else {
                    contentType = OCTET;
                }
            } else if (extensions.length() == 4) {
                if ("jpeg".equals(extensions)) {
                    contentType = JPEG;
                } else {
                    contentType = OCTET;
                }
            } else {
                contentType = OCTET;
            }
        }
        return contentType;
    }
    
    public static boolean containsFile(PostParameter[] params)
    {
        boolean containsFile = false;
        if(null == params){
            return false;
        }
        for(int i=0; i<params.length; i++)
        {
            if (params[i].isFile())
            {
                containsFile = true;
                break;
            }
        }

        return containsFile;
    }
/*    
    static boolean containsFile(List params)
    {
        boolean containsFile = false;
        
        for(int i=0; i<params.)
        for (PostParameter param : params) {
            if (param.isFile()) {
                containsFile = true;
                break;
            }
        }
        return containsFile;
    }
*/
    public static PostParameter[] getParameterArray(String name, String value) {
        return new PostParameter[]{new PostParameter(name,value)};
    }
    public static PostParameter[] getParameterArray(String name, int value) {
        return getParameterArray(name,String.valueOf(value));
    }

    public static PostParameter[] getParameterArray(String name1, String value1
            , String name2, String value2) {
        return new PostParameter[]{new PostParameter(name1, value1)
                , new PostParameter(name2, value2)};
    }
    public static PostParameter[] getParameterArray(String name1, int value1
            , String name2, int value2) {
        return getParameterArray(name1,String.valueOf(value1),name2,String.valueOf(value2));
    }
    
    public int hashCode() {
        int result = name.hashCode();
        result = 31 * result + value.hashCode();
        result = 31 * result + (file != null ? file.hashCode() : 0);
        return result;
    }

    public boolean equals(Object obj) {
        if (null == obj) {
            return false;
        }
        if (this == obj) {
            return true;
        }
        if (obj instanceof PostParameter) {
            PostParameter that = (PostParameter) obj;
            
            if (file != null ? !file.equals(that.file) : that.file != null)
                return false;
            
            return this.name.equals(that.name) &&
                this.value.equals(that.value);
        }
        return false;
    }
   
    public String toString() {
        return "PostParameter{" +
                "name='" + name + '\'' +
                ", value='" + value + '\'' +
                ", file=" + file +
                '}';
    }
    
    public int compareTo(Object o) {
        int compared;
        PostParameter that = (PostParameter) o;
        compared = name.compareTo(that.name);
        if (0 == compared) {
            compared = value.compareTo(that.value);
        }
        return compared;
    }
    
/*    
    public static String encodeParameters(PostParameter[] httpParams) {
        if (null == httpParams) {
            return "";
        }
        StringBuffer buf = new StringBuffer();
        for (int j = 0; j < httpParams.length; j++) {
            if (httpParams[j].isFile()) {
                throw new IllegalArgumentException("parameter [" + httpParams[j].name + "]should be text");
            }
            if (j != 0) {
                buf.append("&");
            }
            try {
                buf.append(new URLEncodedPostData(httpParams[j].name, "UTF-8")).append("=").append(new URLEncodedPostData(httpParams[j].value, "UTF-8"));
			} catch (Exception e) {}
        }
        return buf.toString();
    }
*/    
}