package weibo.model;

import code.StringUtility;

public class Source
{
    private String url;               //来源连接
    private String relationShip;      
    private String name;              //来源文案名称
    
	public Source(String str)
	{
		super();
		String[] source = StringUtility.split(str, "\"");
        url = source[1];
        relationShip = source[3];
        name = StringUtility.replace(StringUtility.replaceAll(source[4], ">", ""), "</a", "");
	}
    
	public String getUrl() {return url;}
	public String getRelationship() {return relationShip;}
	public String getName() {return name;}
	
	public void setUrl(String url) {this.url = url;}
	public void setRelationship(String relationShip) {this.relationShip = relationShip;}
	public void setName(String name) {this.name = name;}

	public String toString()
	{
		return "\n\u2605Source [" + 
			   "\n\u2022 url=" + url + 
			   "\n\u2022 relationShip=" + relationShip +
			   "\n\u2022 name=" + name + 
			   "\n\u2022]";
	}
}