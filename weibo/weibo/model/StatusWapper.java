package weibo.model;

import java.util.Vector;

public class StatusWapper
{
	private Vector statuses;

	private long previousCursor;

	private long nextCursor;

	private long totalNumber;
	
	private String hasvisible;

	public StatusWapper(Vector statuses, long previousCursor, long nextCursor, long totalNumber,String hasvisible)
	{
		this.statuses = statuses;
		this.previousCursor = previousCursor;
		this.nextCursor = nextCursor;
		this.totalNumber = totalNumber;
		this.hasvisible = hasvisible;
	}

	public StatusWapper(Vector statuses)
	{
		this.statuses=statuses;
	}
	
	public Vector getStatuses() {return statuses;}
	public long getPreviousCursor() {return previousCursor;}
	public long getNextCursor() {return nextCursor;}
	public long getTotalNumber() {return totalNumber;}
	public String getHasvisible() {return hasvisible;}
	
	public void setStatuses(Vector statuses) {this.statuses = statuses;}
	public void setPreviousCursor(long previousCursor) {this.previousCursor = previousCursor;}
	public void setNextCursor(long nextCursor) {this.nextCursor = nextCursor;}
	public void setTotalNumber(long totalNumber) {this.totalNumber = totalNumber;}
	public void setHasvisible(String hasvisible) {this.hasvisible = hasvisible;}
}