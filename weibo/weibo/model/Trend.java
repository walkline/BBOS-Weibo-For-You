package weibo.model;

import org.json.me.JSONException;
import org.json.me.JSONObject;

public class Trend
{
    private String name;
    private String query = null;
    private long amount;
    private long delta;

    public Trend(JSONObject json) throws JSONException
    {
        this.name = json.getString("name");
        if (!json.isNull("query")) {this.query = json.getString("query");}
        this.amount =json.getLong("amount");
        this.delta = json.getLong("delta");
    }

    public String getName() {return name;}

    public String getQuery() {return query;}

    public long getAmount() {return amount;}

	public void setAmount(long amount) {this.amount = amount;}

	public long getDelta() {return delta;}

	public void setDelta(long delta) {this.delta = delta;}

	public void setName(String name) {this.name = name;}

	public void setQuery(String query) {this.query = query;}

	public boolean equals(Object o)
	{
        if (this == o) return true;
        if (!(o instanceof Trend)) return false;

        Trend trend = (Trend) o;

        if (!name.equals(trend.name)) return false;
        if (query != null ? !query.equals(trend.query) : trend.query != null)
            return false;

        return true;
    }

    public int hashCode()
    {
        int result = name.hashCode();
        result = 31 * result + (query != null ? query.hashCode() : 0);
        return result;
    }

	public String toString()
	{
		return "Trend [name=" + name + ", query=" + query + ", amount="
				+ amount + ", delta=" + delta + "]";
	}   
}