package weibo.model;

import java.util.Vector;

import org.json.me.JSONArray;
import org.json.me.JSONException;
import org.json.me.JSONObject;

import weibo.http.Response;

/**
 * 话题
 */
public class UserTrend extends WeiboResponse
{
    private String num;               
    private String hotword = null;
    private String trendId = null;
 
	public UserTrend() {super();}
	
	public UserTrend(Response res) throws WeiboException
	{
		super(res);
		JSONObject json = res.asJSONObject();
		try {
			num = json.getString("num");
			hotword = json.getString("hotword");
			trendId = json.getString("trend_id");
			if( json.getString("topicid")!=null)
				trendId = json.getString("topicid");
		} catch (JSONException je) {throw new WeiboException(je.getMessage() + ":" + json.toString());}
	}
	
	public UserTrend(JSONObject json) throws WeiboException
	{
		try {
			num = json.getString("num");
			hotword = json.getString("hotword");
			trendId = json.getString("trend_id");
		} catch (JSONException je) {throw new WeiboException(je.getMessage() + ":" + json.toString());}
	}
	
	public static Vector constructTrendList(Response res) throws WeiboException
	{
	   	 try {
	            JSONArray list = res.asJSONArray();
	            int size = list.length();
	            Vector trends = new Vector(size);
	            for (int i = 0; i < size; i++) {
	            	trends.addElement(new UserTrend(list.getJSONObject(i)));
	            }
	            return trends;
	        } catch (JSONException jsone) {throw new WeiboException(jsone.toString());}
	   	      catch (WeiboException te) {throw te;}
	   }
	
	public String getNum() {return num;}
	public String getHotword() {return hotword;}
	public String gettrendId() {return trendId;}	
	
	public void setNum(String num) {this.num = num;}
	public void setHotword(String hotword) {this.hotword = hotword;}
	public void settrendId(String trendId) {this.trendId = trendId;}
	
	public String toString()
	{
		return "Trend [num=" + num + ", hotword=" + hotword + ", trendId=" + trendId + "]";
	}
}