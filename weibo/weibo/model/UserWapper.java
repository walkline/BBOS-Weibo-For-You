package weibo.model;

import java.util.Vector;

/**
 * 对User对象列表进行的包装，以支持cursor相关信息传递
 * @author sinaWeibo
 */
public class UserWapper
{
	private Vector users;
	private long previousCursor;
	private long nextCursor;
	private long totalNumber;
	private String hasvisible;

	public UserWapper(Vector users, long previousCursor, long nextCursor, long totalNumber,String hasvisible)
	{
		this.users = users;
		this.previousCursor = previousCursor;
		this.nextCursor = nextCursor;
		this.totalNumber=totalNumber;
		this.hasvisible = hasvisible;
	}

	public UserWapper(Vector users)
	{
		this.users=users;
	}
	
	public Vector getUsers() {return users;}
	public long getPreviousCursor() {return previousCursor;}
	public long getNextCursor() {return nextCursor;}
	public long getTotalNumber() {return totalNumber;}
	public String getHasvisible() {return hasvisible;}

	public void setUsers(Vector users) {this.users = users;}
	public void setPreviousCursor(long previousCursor) {this.previousCursor = previousCursor;}
	public void setNextCursor(long nextCursor) {this.nextCursor = nextCursor;}
	public void setTotalNumber(long totalNumber) {this.totalNumber = totalNumber;}
	public void setHasvisible(String hasvisible) {this.hasvisible = hasvisible;}
}