package weibo.model;

import org.json.me.JSONException;
import org.json.me.JSONObject;

public class Visible
{
	private int type;
	private int list_id;
	
	public Visible(JSONObject json) throws JSONException
	{
		this.type = json.getInt("type");
		this.list_id = json.getInt("list_id");
	}
	
	public int getType() {return type;}
	public int getList_id() {return list_id;}
	public void setType(int type) {this.type = type;}
	public void setList_id(int list_id) {this.list_id = list_id;}
	
	public int hashCode()
	{
		final int prime = 31;
		int result = 1;
		result = prime * result + list_id;
		result = prime * result + type;
		return result;
	}
	
	public boolean equals(Object obj)
	{
		if(this == obj) return true;
		if(obj == null) return false;
		if(getClass() != obj.getClass()) return false;
		
		Visible other = (Visible) obj;
		if(list_id != other.list_id) return false;
		if(type != other.type) return false;
		
		return true;
	}
	
	public String toString()
	{
		return "\n\u2605Visible [" +
			   "\n\u2022 type=" + type + 
			   "\n\u2022 list_id=" + list_id + 
			   "\n\u2022]";
	}
}