package weibo.model;

import org.json.me.JSONException;
import org.json.me.JSONObject;

public class WeiboException extends Exception
{
    private int statusCode = -1;
    private int errorCode = -1;
    private String request;
    private String error;

    public WeiboException(String msg) {super(msg);}

    //public WeiboException(Exception cause) {super(cause);}
    
    public WeiboException(String msg , int statusCode) throws JSONException
    {
    	super(msg);
    	this.statusCode = statusCode;
    }

    public WeiboException(String msg , JSONObject json, int statusCode) throws JSONException 
    {
        super(msg + "\n error:" + json.getString("error") +" error_code:" + json.getInt("error_code") + json.getString("request"));
        
        this.statusCode = statusCode;
        this.errorCode = json.getInt("error_code");
        this.error = json.getString("error");
        this.request = json.getString("request");
    }

    //public WeiboException(String msg, Exception cause) {super(msg, cause);}

    //public WeiboException(String msg, Exception cause, int statusCode) {
    //    super(msg, cause);
    //    this.statusCode = statusCode;
    //}

    public int getStatusCode() {return this.statusCode;}
	public int getErrorCode() {return errorCode;}
	public String getRequest() {return request;}
	public String getError() {return error;}
}